/**
 * Created by Max Kostin on 26/10/2016.
 */


///<reference path="actors/Actor.ts" />
///<reference path="events/Dispatcher.ts" />
///<reference path="actors/MeshFactory.ts" />
///<reference path="events/VTEvent.ts" />
///<reference path="SoundManager.ts" />

module vt {

    import Dispatcher = vt.events.Dispatcher;
    import Event = vt.events.VTEvent;
    import Actor = vt.actors.Actor;
    import MeshFactory = vt.actors.MeshFactory;
    import VTEvent = vt.events.VTEvent;
    import Snowman = vt.actors.Snowman;
    import Container = vt.actors.Container;

    export class GameController extends vt.events.Dispatcher {

        private engine: any;
        private scene;
        private canvas: HTMLCanvasElement;
        private clonableMeshes: any = {};
        private actors: Actor[];
        private collisionActors: Actor[];
        private collectables: any[];
        private camera;

        private snowEmiter;
        private snowEmiterSize = 50;
        private snowEmiterY = 10;

        private _isTouchDevice: boolean = false;

        private _isDebugMode: boolean = false;
        private _isDebugChecked: boolean = false;

        private soundManager: vt.SoundManager;

        private exit;

        private w: number;
        private h: number;


        private balls = [];
        private pressTime = 0;

        private hl;
        private _setupDone: boolean = false;

        private isWalkSoundPlaying: boolean = false;

        private pointerDown = "mousedown";
        private pointerUp = "mouseup";
        private pointerMove = "mousemove";


        private static _instance: GameController;

        constructor() {

            super();
            GameController._instance = this;
            this.canvas = <HTMLCanvasElement>document.getElementById("canvas");
            this.soundManager = vt.SoundManager.getInstance();
            this.resizeHandler = this.resizeHandler.bind(this);
            window.addEventListener("resize", this.resizeHandler);

            this.setControls();

            GameController.setRay();
        }

        public static getInstance(): GameController {
            return GameController._instance;
        }


        /**
         * =============================================================================================================
         * Main game loop
         * =============================================================================================================
         */
        private run(): void {

            var x = this.camera.position.x;
            var y = this.camera.position.y;
            var z = this.camera.position.z;

            this.camera.cbox.position.x = x;
            this.camera.cbox.position.y = y;
            this.camera.cbox.position.z = z;
            this.camera.cbox.computeWorldMatrix();

            this.snowEmiter.position.x = x;
            this.snowEmiter.position.z = z;

            for (var i = 0; i < this.actors.length; i++) {
                this.actors[i].update();
            }

            this.camera.rotation.y += this.radY;
            this.camera.rotation.x += this.radX;

            if (this.camera._localDirection) {
                this.camera._localDirection.copyFromFloats(this.moveX, 0, -this.moveZ);
                this.camera.getViewMatrix().invertToRef(this.camera._cameraTransformMatrix);
                BABYLON.Vector3.TransformNormalToRef(this.camera._localDirection, this.camera._cameraTransformMatrix, this.camera._transformedDirection);
                this.camera.cameraDirection.addInPlace(this.camera._transformedDirection);
            }

            var rx = this.camera.rotation.x;
            if (rx <= -.35) {
                this.camera.rotation.x = -.35;
            } else if (rx >= .25) {
                this.camera.rotation.x = .25;
            }

            this.checkPickups();
            this.checkExit();
            this.checkBalls();

            this.pressTime++;

            var speed = (Math.abs(this.camera._oldPosition.z - this.camera._newPosition.z) + Math.abs(this.camera._oldPosition.x - this.camera._newPosition.x));
            if (!this.isWalkSoundPlaying && speed > 0.1) {
                this.soundManager.playerWalk();
                this.isWalkSoundPlaying = true;
            } else if (this.isWalkSoundPlaying && speed < 0.1) {
                this.soundManager.stopPlayerWalk();
                this.isWalkSoundPlaying = false;
            }


            //console.log(speed)

            //this.camera.rotation.y += (-this.ry - this.camera.rotation.y) / 6;
            //this.camera.rotation.x += (-this.rx - this.camera.rotation.x) / 6;

        }

        public destroy(): void {
            if (this.scene) {
                this.scene.onPointerUp = this.scene.onPointerDown = null;
                this.scene.dispose();
            }
        }

        /*private mouseMoveHandler(e): void {
         if (!this.camera) {
         return;
         }
         this.ry = (this.w - e.clientX) / this.w * Math.PI * 2;
         this.rx = (this.h - e.clientY) / this.h * Math.PI;

         }*/


        /**
         * =============================================================================================================
         * Initialises level
         * =============================================================================================================
         * @param scene
         * @param engine
         * @param data
         * @param startEngine
         */

        public initLevel(scene: any, engine: any, data: JSON, startEngine: boolean = true): void {

            //this.updateScreenSize();

            var camera;
            //if (! this._setupDone) {
            if (this._isTouchDevice) {
                camera = new BABYLON.FreeCamera("Cam", new BABYLON.Vector3(0, .5, -20), scene);
                //camera.panningSensibility = 50;
                //camera.angularSensibility = 50;
                camera.inertia = 0.5;
                this.pointerDown = "touchstart";
                this.pointerUp = "touchend";
                this.pointerMove = "touchmove";
            } else {
                camera = new BABYLON.UniversalCamera("Cam", new BABYLON.Vector3(0, .5, -20), scene);
                camera.attachControl(this.canvas, false, false);
            }

            camera.fov = .6;

            camera.checkCollisions = true;
            camera.applyGravity = true;
            camera.ellipsoid = new BABYLON.Vector3(1, .5, 1);

            camera.cbox = BABYLON.Mesh.CreateBox("cbox", 1, scene);
            camera.cbox.scaling = new BABYLON.Vector3(.5, 2.1, .5);
            camera.cbox.isPickable = true;
            camera.cbox.isVisible = false;
            //camera.cbox.position.y = .2;
            //camera.cbox.parent = camera;

            camera.position.y = 2;

            camera.keysUp = [87, 38]; // W
            camera.keysDown = [83, 40]; // S
            camera.keysLeft = [65, 37]; // A
            camera.keysRight = [68, 39]; // D

            camera.speed = 1.5;
            //camera.attachControl(this.canvas, false, false);
            this.camera = camera;
            // }

            scene.activeCamera = this.camera;


            scene.clearColor = new BABYLON.Color3(.4, .45, .5);
            scene.ambientColor = new BABYLON.Color3(.2, .2, .2);


            //BABYLON.SceneOptimizer.OptimizeAsync(scene);


            var ground = scene.getMeshByName("ground");

            scene.gravity = new BABYLON.Vector3(0, -0.9, 0);
            scene.collisionsEnabled = true;


            ground.checkCollisions = true;

            this.snowEmiter = BABYLON.Mesh.CreateGround("snow-emmiter", this.snowEmiterSize, this.snowEmiterSize, 1, scene);
            this.snowEmiter.position.y = this.snowEmiterY;

            this.scene = scene;
            this.engine = engine;

            this.listClonableMeshes();
            this.parseLevel(data);


            if (startEngine) {
                this.startEngine();
            }

            if (!this._setupDone) {
            }
            this.setThrowBalls();

            this._isDebugMode = window.location.hash === "#d";

            if (this._isDebugMode && !this._isDebugChecked) {
                scene.debugLayer.show();
                this._isDebugChecked = true;
            }

            //console.log(scene.workerCollisions)
            scene.workerCollisions = true;

            this._setupDone = true;

            document.getElementById("canvas").focus();

        }


        public getCollisionActors(): Actor[] {
            return this.collisionActors;
        }

        public pause(): void {
            this.engine.stopRenderLoop();
        }

        /*public unpause(): void {
         this.engine.runRenderLoop(()=> {
         this.scene.render();
         });
         }*/

        public playerAddPoints(value): void {
            this.dispatch(new VTEvent(VTEvent.POINTS_CHANGE, value));
        }

        public set isTouchDevice(value: boolean) {
            this._isTouchDevice = value;
        }

        public get isTouchDevice(): boolean {
            return this._isTouchDevice;
        }


        private startEngine() {

            this.scene.registerBeforeRender(() => this.run());

            this.engine.runRenderLoop(() => {
                this.scene.render();
            });
        }


        private parseLevel(json) {

            this.actors = [];
            this.collisionActors = [];
            this.collectables = [];

            this.hl = new BABYLON.HighlightLayer("hl1", this.scene);
            this.hl.blurHorizontalSize = 1;
            this.hl.blurVerticalSize = 1;

            for (var i = 0; i < json.items.length; i++) {

                var obj: any = json.items[i];
                var clone;

                switch (obj.type) {

                    case "camera" :
                        var cam = this.scene.activeCamera;
                        cam.position.x = obj.pos.x;
                        cam.position.z = obj.pos.z;
                        break;

                    case "container" :
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        clone = GameController.getClone(mesh, obj, i + "", true, "instance");
                        clone.isPickable = true;
                        var actor = MeshFactory.getActor(clone, obj.type, this.scene);
                        this.actors.push(actor);
                        this.collisionActors.push(actor);


                        var rand = Math.floor(Math.random() * 2);
                        if (rand === 1) {
                            clone.rotation.y += Math.PI;
                        }

                        //clone.material.alpha = .2;
                        //clone.scaling.y = 5;
                        //clone.computeWorldMatrix();

                        if (obj.kind === "stack") {
                            for (var j = 1; j < 3; j++) {
                                var mesh2 = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                                var clone2 = GameController.getClone(mesh2, obj, j + "s", false);
                                clone2.position.y += j * 4;
                                var actor2 = MeshFactory.getActor(clone2, obj.type, this.scene);
                                this.actors.push(actor2);
                                this.collisionActors.push(actor2);
                            }
                        }

                        break;

                    case "snowman" :
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        mesh.computeBonesUsingShaders = false;
                        var clone: any = GameController.getClone(mesh, obj, i + "", false, "clone");
                        clone.pos = clone.position.clone();
                        clone.skeleton = mesh.skeleton.clone("skeleton" + i);
                        var actor = MeshFactory.getActor(clone, obj.type, this.scene);
                        this.actors.push(actor);
                        clone.computeWorldMatrix();
                        break;

                    case "crane" :
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone: any = GameController.getClone(mesh, obj, i + "", true, "instance");
                        var actor = MeshFactory.getActor(clone, obj.type, this.scene, obj);
                        actor.initActor();
                        this.actors.push(actor);
                        setTimeout(() => {
                            this.soundManager.playCrane(actor.mesh);
                        }, 700);
                        break;

                    case "crane2" :
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone: any = GameController.getClone(mesh, obj, i + "", true, "clone");
                        break;

                    case "collectable" :
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone: any = GameController.getClone(mesh, obj, i + "", false, "instance");
                        clone.rotation.y = Math.random();
                        clone.presentValue = 15;
                        this.collectables.push(clone);
                        break;

                    case "collectableBigger" :
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone: any = GameController.getClone(mesh, obj, i + "", false, "clone");
                        clone.rotation.y = Math.random();
                        clone.presentValue = 100;
                        this.hl.addMesh(clone, BABYLON.Color3.White());
                        this.collectables.push(clone);
                        break;

                    case "exit" :
                        var exit = this.scene.getMeshByName("exit");
                        //this.hl.addMesh(exit, BABYLON.Color3.Green());
                        exit.position.x = obj.pos.x;
                        exit.position.z = obj.pos.z;
                        this.exit = exit;
                        var actor = MeshFactory.getActor(exit, obj.type, this.scene);
                        this.actors.push(actor);
                        setTimeout(() => {
                            this.soundManager.playCloseToExit(exit.position);
                        }, 700);
                        break;

                    case "announcer" :
                        this.soundManager.makeAnouncer(obj.pos, false);
                        mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone: any = GameController.getClone(mesh, obj, i + "", false, "instance");
                        break;

                    case "truck" :
                        if (! this.isTouchDevice) {
                            var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                            var clone: any = GameController.getClone(mesh, obj, i + "", true, "instance");
                            var actor = MeshFactory.getActor(clone, obj.type, this.scene);
                            this.actors.push(actor);
                            this.collisionActors.push(actor);
                        }
                        break;

                }

            }

            this.makeFog(json.fog);
            this.makeSnow();
            this.dispatch(new Event(Event.SCENE_READY, {scene: this.scene, engine: this.engine}, this));
            this.scene.getMeshByName("item-snowman").dispose();
            if (this.isTouchDevice) {
                this.scene.getMeshByName("item-truck").dispose();
            } else {
                this.scene.getMeshByName("item-truck").setEnabled(false);
            }
            this.hl.addMesh(this.scene.getMeshByName("star"), BABYLON.Color3.Yellow());
            console.log("collision meshes:" + this.collisionActors.length);

            //var merged = BABYLON.Mesh.MergeMeshes(containers);
        }

        private static getClone(mesh, obj, name: string, checkCollisions: boolean = true, type: string = "instance"): any {
            var clone;
            if (type === "instance") {
                clone = mesh.createInstance(mesh.name + "_" + name);
            } else {
                clone = mesh.clone(mesh.name + "_" + name);
            }
            clone.position.x = obj.pos.x;
            clone.position.z = obj.pos.z;
            clone.rotation.y = obj.rotation.y;
            clone.checkCollisions = checkCollisions;
            clone.scaling.x = 1;
            clone.scaling.y = 1;
            clone.scaling.z = 1;
            return clone;
        }

        private listClonableMeshes() {

            var list = this.scene.meshes;
            this.clonableMeshes = {};

            for (var i = 0; i < list.length; i++) {
                var mesh = list[i];
                if (mesh.name.indexOf("item-") > -1) {
                    var name = mesh.name.split("-")[1];
                    if (!this.clonableMeshes.hasOwnProperty(name)) {
                        this.clonableMeshes[name] = [];
                    }
                    this.clonableMeshes[name].push(mesh);
                    mesh.setEnabled(false);
                }
            }
        }


        private makeSnow(): void {
            var particleSystem = new BABYLON.ParticleSystem("particles", 2000, this.scene);
            particleSystem.particleTexture = new BABYLON.Texture("assets/snowflake.png", this.scene);
            particleSystem.emitter = this.snowEmiter;
            particleSystem.emitRate = 500;
            particleSystem.minEmitBox = new BABYLON.Vector3(-this.snowEmiterSize, 0, -this.snowEmiterSize); // Starting all From
            particleSystem.maxEmitBox = new BABYLON.Vector3(this.snowEmiterSize, 0, this.snowEmiterSize); // To...
            particleSystem.gravity = new BABYLON.Vector3(0, -5, 0);
            particleSystem.minSize = 0.08;
            particleSystem.maxSize = 0.2;
            particleSystem.minLifeTime = 2;
            particleSystem.maxLifeTime = 5;
            particleSystem.direction1 = new BABYLON.Vector3(0, -8, 0);
            particleSystem.direction2 = new BABYLON.Vector3(0, -8, 0);
            particleSystem.start();
        }

        private makeFog(fogFar: number = 90.0): void {
            this.scene.fogMode = BABYLON.Scene.FOGMODE_LINEAR;
            this.scene.fogStart = 10.0;
            this.scene.fogEnd = fogFar;
            this.scene.fogColor = new BABYLON.Color3(0.4, 0.45, 0.5);
        }


        private checkPickups(): void {

            for (var i = 0; i < this.collectables.length; i++) {
                var item = this.collectables[i];
                if (item.getDistanceToCamera(this.camera) < 2) {
                    this.dispatch(new VTEvent(VTEvent.POINTS_CHANGE, item.presentValue));
                    this.collectables.splice(i, 1);
                    this.soundManager.pickPresent();
                    item.dispose();
                }
            }
        }

        private checkExit(): void {
            if (this.exit && this.exit.getDistanceToCamera(this.camera) < 5) {
                this.dispatch(new VTEvent(VTEvent.LEVEL_COMPLETE));
            }
        }


        /*private static getDistance(mesh1, mesh2): number {
         return ( (mesh1.position.x - mesh2.position.x) * (mesh1.position.x - mesh2.position.x) + (mesh1.position.z - mesh2.position.z) * (mesh1.position.z - mesh2.position.z) );
         }*/


        private static getRand(num): number {
            return Math.floor(Math.random() * num);
        }


        private resizeHandler(): void {
            if (this.engine) this.engine.resize();
            this.updateScreenSize();
        }

        private updateScreenSize(): void {
            this.w = window.innerWidth / 2;
            this.h = window.innerHeight / 2;
        }


        private setThrowBalls(): void {

            var wall = BABYLON.Mesh.CreatePlane("shootTarget", 200.0, this.scene);
            wall.material = new BABYLON.StandardMaterial("$texture1", this.scene);
            wall.material.alpha = 0;
            //wall.isVisible = false;
            wall.position.z = 10;
            wall.parent = this.camera;
            this.hl.addExcludedMesh(wall);


            this.scene.onPointerObservable.add(() => {
                this.pressTime = 0;

            }, BABYLON.PointerEventTypes.POINTERDOWN);


            this.scene.onPointerObservable.add((evt) => {
                if (this.pressTime < 12 && evt.pickInfo.hit) {
                    var b = this.scene.getMeshByName("item-snowball").createInstance("sss");
                    b.position.copyFrom(this.camera.position);
                    b.lookAt(evt.pickInfo.pickedPoint);
                    b.gravity = .05;
                    b.speed = 1;
                    b.time = 150;
                    this.balls.push(b);
                    this.soundManager.throwBall();
                }
            }, BABYLON.PointerEventTypes.POINTERUP);


        }

        private checkBalls(): void {
            for (var i = 0; i < this.balls.length; i++) {
                var b = this.balls[i];
                b.translate(BABYLON.Axis.Z, -b.speed, BABYLON.Space.LOCAL);
                b.translate(BABYLON.Axis.Y, b.gravity, BABYLON.Space.LOCAL);
                b.gravity -= .004;
                b.speed -= .008;
                b.time--;
                if (b.time < 0) {
                    this.removeBall(b);
                }
                for (var j = 0; j < this.actors.length; j++) {
                    if (b.intersectsMesh(this.actors[j].mesh)) {

                        if (this.actors[j] instanceof Snowman) {
                            this.actors[j].hit();
                            this.soundManager.snowmanHit(this.actors[j].mesh);
                        }
                        this.removeBall(b);
                    }
                }
            }
        }

        private removeBall(b): void {
            this.balls.splice(this.balls.indexOf(b), 1);
            b.dispose();
        }

        public removeActor(actor: Actor): void {
            var ind = this.actors.indexOf(actor);
            if (ind > -1) {
                this.actors.splice(ind, 1);
            }
        }


        private divMove;
        private divRotate;
        private rotPosX0 = 0;
        private rotPosY0 = 0;
        private radY = 0;
        private radX = 0;
        private movePosX0 = 0;
        private movePosY0 = 0;
        private moveX = 0;
        private moveZ = 0;

        private setControls(): void {

            this.rotStart = this.rotStart.bind(this);
            this.rotRun = this.rotRun.bind(this);
            this.rotEnd = this.rotEnd.bind(this);

            this.moveStart = this.moveStart.bind(this);
            this.moveRun = this.moveRun.bind(this);
            this.moveEnd = this.moveEnd.bind(this);

            this.divMove = document.getElementById("c-move");
            this.divRotate = document.getElementById("c-rotate");

            this.divRotate.addEventListener("touchstart", this.rotStart);
            this.divRotate.addEventListener("touchend", this.rotEnd);

            this.divMove.addEventListener("touchstart", this.moveStart);
            this.divMove.addEventListener("touchend", this.moveEnd);

        }

        private rotStart(e): void {
            this.rotPosX0 = e.touches[0].clientX;
            this.rotPosY0 = e.touches[0].clientY;
            this.divRotate.addEventListener("touchmove", this.rotRun);
            e.preventDefault();
        }

        private rotRun(e): void {
            e.preventDefault();
            if (!this.camera) return;
            var touch = e.touches[0];

            var speedY = (touch.clientX - this.rotPosX0) / 700;
            var speedYAbs = Math.abs(speedY);
            if (speedYAbs > .04 && speedY > 0) {
                speedY = .04;
            } else if (speedYAbs > .04 && speedY < 0) {
                speedY = -.04;
            }
            this.radY = speedY;


            var speedX = (touch.clientY - this.rotPosY0) / 2000;
            var speedXAbs = Math.abs(speedX);
            if (speedYAbs < 0.01) {
                if (speedXAbs > .03 && speedX > 0) {
                    speedX = .03;
                } else if (speedXAbs > .04 && speedX < 0) {
                    speedX = -.04;
                }

                //this.radX = speedX;
            }

        }

        private rotEnd(e): void {
            this.divRotate.removeEventListener("touchmove", this.rotRun);
            this.radX = 0;
            this.radY = 0;
        }


        private moveStart(e): void {
            this.movePosX0 = e.touches[0].clientX;
            this.movePosY0 = e.touches[0].clientY;
            this.divMove.addEventListener("touchmove", this.moveRun);
            e.preventDefault();
        }

        private moveRun(e): void {
            e.preventDefault();
            if (!this.camera) return;
            var touch = e.touches[0];

            var speedZ = (touch.clientY - this.movePosY0) / 70;
            var speedAbsZ = Math.abs(speedZ);
            this.moveZ = speedZ;

            var speedX = (touch.clientX - this.movePosX0) / 100;
            var speedXAbs = Math.abs(speedX);

            if (speedAbsZ < 0.2) {
                this.moveX = speedX;
                //this.moveZ = 0;
            }


        }

        private moveEnd(e): void {
            this.divMove.removeEventListener("touchmove", this.moveRun);
            this.moveX = 0;
            this.moveZ = 0;
        }



        private static setRay(): void {

            var R = BABYLON.Ray;

            R.prototype.rayLine = null;

            R.prototype.intersectsMesh = function (mesh, fastCheck) {
                var ray = BABYLON.Ray.Transform(this, mesh.getWorldMatrix().clone().invert());
                return mesh.intersects(ray, fastCheck);
            };

            R.prototype.intersectsMeshes = function (meshes, fastCheck) {
                var arr = [];
                for (var i = 0; i < meshes.length; i++) {
                    var ray = BABYLON.Ray.Transform(this, meshes[i].getWorldMatrix().clone().invert());
                    var pick = meshes[i].intersects(ray, fastCheck);
                    if (pick.hit) {
                        arr.push(pick);
                    }
                }

                arr.sort(R.pickSortFunc);
                return arr;
            };

            R.pickSortFunc = function (a, b) {
                if (a.distance > b.distance) {
                    return 1;
                } else if (a.distance < b.distance) {
                    return -1;
                }
                return 0;
            };

            R.prototype.show = function (scene, color) {
                var ps = [this.origin, this.origin.add(this.direction.scale(this.length))];
                this.rayLine = BABYLON.Mesh.CreateLines("rayLine", ps, scene, true);
                this.rayLine.color = color || new BABYLON.Color3(1, 0, 0);
            };

            R.prototype.update = function () {
                if (this.rayLine) {
                    this.rayLine = BABYLON.Mesh.CreateLines(null, [this.origin, this.origin.add(this.direction.scale(this.length))], null, null, this.rayLine);
                }
            };

            R.prototype.setDirectionFromMesh = function (mesh) {
                this.direction = mesh.getDirection(new BABYLON.Vector3(0, 0, -1));
            }

        }


    }

}








