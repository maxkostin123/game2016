/**
 * Created by Max Kostin on 26/10/2016
 */

///<reference path="MeshFactory.ts" />
///<reference path="../GameController.ts" />
///<reference path="../SoundManager.ts" />

module vt.actors {


    export class Snowman extends Actor {

        private _health = 9;

        private balls: any[] = [];
        //private wmat;
        private bone;

        private collisionActors: Actor[];


        private canThrow: boolean = true;
        private timeOut;
        private _isAlive: boolean = true;

        private ray: any;
        private pose;


        constructor(mesh: any, scene: any) {
            super(mesh, scene);
            this.init();
        }

        private init(): void {

            //this.wmat = BABYLON.Matrix.Identity();


            this.collisionActors = vt.GameController.getInstance().getCollisionActors();
            this.mesh.checkCollisions = false;
            this.scene.beginAnimation(this.mesh.skeleton, 0, 1, 1.0, false);
            this.bone = this.scene.getBoneByName("bone-rHand");

            this.mesh.computeBonesUsingShaders = !GameController.getInstance().isTouchDevice;


            this.mesh.lookAt(this.camera.position);
            this.pose = this.mesh.position.clone();
            this.pose.y = 1;
            var rot = this.mesh._rotationQuaternion.toEulerAngles().clone();
            rot.x = 0;

            this.ray = new BABYLON.Ray(this.pose, this.mesh.getDirection(new BABYLON.Vector3(0,0,-1)), 90);
            //this.ray.show(this.scene);

            for (var i = 0; i < this.collisionActors.length; i++) {
                var box = this.collisionActors[i].mesh._boundingInfo.boundingBox;
                //console.log(this.r2.ray.intersectsBox(box));
            }
        }


        private scan(): boolean {

            var box = this.camera.cbox;

            /*
            this.ray.setDirectionFromMesh(this.mesh);
            this.ray.direction.y = 0;
            this.ray.update();
            console.log(this.ray.intersectsMesh(box, true))
            */

            return this.mesh.getDistanceToCamera(this.camera) < 100;

            //return false;


        }

        public destroy(): void {
            super.destroy();
            for (var i = 0; i < this.balls.length; i++) {
                this.balls[i].dispose();
            }
        }

        public hit(): void {
            this._health -= 3;
            this.makeSnow();
            if (this._health <= 0) {
                this.makeSnow(500, -2);
                this._isAlive = false;
                this.mesh.rotation.z = Math.PI / 2;
                GameController.getInstance().playerAddPoints(500);
            } else {
                this.makeSnow(30);
            }
        }

        private makeSnow(numParticles: number = 80, gravity: number = -10): void {
            var particleSystem = new BABYLON.ParticleSystem("particles", 200, this.scene);
            particleSystem.particleTexture = new BABYLON.Texture("assets/snowflake.png", this.scene);
            particleSystem.minEmitBox = new BABYLON.Vector3(-.5, .5, -.5); // Starting all From
            particleSystem.gravity = new BABYLON.Vector3(0, gravity, 0);
            particleSystem.color1 = new BABYLON.Color4(0.7, 0.8, 1.0, 1.0);
            particleSystem.color2 = new BABYLON.Color4(0.2, 0.5, 1.0, 1.0);
            particleSystem.emitter = this.mesh;
            particleSystem.manualEmitCount = numParticles;
            particleSystem.maxEmitBox = new BABYLON.Vector3(.5, 1, .5); // To...
            particleSystem.minSize = 0.1;
            particleSystem.maxSize = 0.3;
            particleSystem.minLifeTime = 1;
            particleSystem.maxLifeTime = 2;
            particleSystem.direction1 = new BABYLON.Vector3(-2, 2, -2);
            particleSystem.direction2 = new BABYLON.Vector3(5, 5, 5);
            particleSystem.minAngularSpeed = -Math.PI;
            particleSystem.maxAngularSpeed = Math.PI;
            particleSystem.disposeOnStop = true;
            particleSystem.start();
        }

        public update(): void {

            if (this._isAlive){
                this.mesh.lookAt(this.camera.position.clone());
            }

            for (var i = this.balls.length - 1; i >= 0; i--) {

                var b = this.balls[i];

                b.time++;
                b.translate(BABYLON.Axis.Z, -b.speed, BABYLON.Space.LOCAL);

                if (b.time > 200) {
                    this.removeBall(b);
                }
                /// Snowball hits the player
                if (b.intersectsMesh(this.camera.cbox)) {
                    SoundManager.getInstance().playerHit();
                    this.removeBall(b);
                    GameController.getInstance().playerAddPoints(-30);
                    T._(this.camera.rotation, .09, [{prop: "x", to: -20 * Math.PI / 180}], {yoyo: true, repeat: 1, call:()=> {
                        this.camera.rotation.x = 0;
                    }});
                }

                for (var j = 0; j < this.collisionActors.length; j++) {
                    var actorMesh = this.collisionActors[j].mesh;
                    if (actorMesh != this.mesh && actorMesh.intersectsPoint(b.position)) {
                        this.removeBall(b);
                    }
                }
                if (!this._isAlive && this.balls.length === 0) {
                    GameController.getInstance().removeActor(this);
                }

            }

            if (this._isAlive && this.scan() && this.canThrow) {
                this.attack();
            }

        }

        private attack(): void {
            this.canThrow = false;
            this.mesh.skeleton.beginAnimation("ArmatureAction.001", false, 1, ()=> {
                //// end animation
                SoundManager.getInstance().snowmanThrow(this.mesh);
                this.timeOut = window.setTimeout(()=> {
                    this.canThrow = true;
                }, Math.floor(Math.random() * 500) + 500);

            });
            window.setTimeout(()=> {
                for (var i = 0; i < 7; i++) {
                    var b = this.makeSnowball();
                    if (i > 0) {

                        b.rotation.y += (Math.random() * 5 - Math.random() * 5) * Math.PI / 180;
                        b.rotation.x += (Math.random() * 3) * Math.PI / 180;

                    }
                }
            }, 800);
        }


        private removeBall(b) {
            for (var i = this.balls.length - 1; i >= 0; i--) {
                if (this.balls[i] === b) {
                    this.balls.splice(i, 1);
                    b.dispose();
                    return;
                }
            }
        }


        private makeSnowball(): any {

            var b = this.scene.getMeshByName("item-snowball").createInstance("b");
            b.scaling = new BABYLON.Vector3(1, 1, 1);


            this.setToBonePos(b.position, this.bone, this.mesh.getWorldMatrix());
            b.position.y += .5;
            b.lookAt(this.camera.position);


            var distPos = this.camera.position.subtract(b.position).normalize();
            distPos = distPos.divide(new BABYLON.Vector3(3, 3, 3));
            b.vect = distPos;
            b.gravity = 0.0;
            b.gravityIncrease = 0.0;
            b.speed = .6 + Math.random() / 5;
            b.time = 0;

            this.balls.push(b);

            return b;

        }


        private setToBonePos(position, bone, meshMat) {

            var wmat = BABYLON.Matrix.Identity();

            var parentBone = bone.getParent();
            wmat.copyFrom(bone.getLocalMatrix());

            if (parentBone) {
                wmat.multiplyToRef(parentBone.getAbsoluteTransform(), wmat);
            }

            wmat.multiplyToRef(meshMat, wmat);

            position.x = wmat.m[12];
            position.y = wmat.m[13];
            position.z = wmat.m[14];

            Snowman.updateBoneMatrix(bone);


        }

        private static updateBoneMatrix(bone) {
            if (bone.getParent()) {
                bone.getLocalMatrix().multiplyToRef(bone.getParent().getAbsoluteTransform(), bone.getAbsoluteTransform());
            }

            var children = bone.children;
            var len = children.length;

            for (var i = 0; i < len; i++) {
                Snowman.updateBoneMatrix(children[i]);
            }
        }


    }

}



/*

 // making box look at a certain point
 var pointToRotateTo = camera.position.clone();

 var axis1 = (box.position).subtract(pointToRotateTo);
 var axis2 = BABYLON.Vector3.Cross(axis1, new BABYLON.Vector3(0, 1, 0));
 var axis3 = BABYLON.Vector3.Cross(axis1, axis2);

 box.rotation = BABYLON.Vector3.RotationFromAxis(axis1, axis2, axis3);

 -------------------------------------------
 var pos1 = this.mesh.position;
 var rot = this.mesh.rotationQuaternion.toEulerAngles();
 var x = Math.sin(rot.x) * 100;
 var y = Math.sin(rot.y) * 1;
 var z = Math.sin(rot.z) * 100;


 BABYLON.Mesh.CreateLines("lines", [
 pos1,
 new BABYLON.Vector3(x, y, z)
 ], this.scene);


 -------------------------------------------
 */













