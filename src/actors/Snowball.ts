/**
    * Created by Max Kostin on 26/10/2016.
    */

module vt.actors {


    export class Snowball extends Actor {

        protected camera:any;

        constructor(mesh:any, scene:any) {

            super(mesh, scene);

        }

        public update():void {

            this.mesh.translate(BABYLON.Axis.Z, -.1, BABYLON.Space.LOCAL);

        }



    }

}



