/**
    * Created by Max Kostin on 26/10/2016.
    */

module vt.actors {


    export class Actor extends vt.events.Dispatcher {

        protected camera:any;
        public mesh;

        constructor(mesh:any, protected scene:any, protected settings:any = null) {
            super();
            this.mesh = mesh;
            this.camera = scene.activeCamera;

        }

        public update():void {

        }

        public initActor():void {

        }

        public hit() {

        }

        public destroy():void {
            this.camera = undefined;
            this.mesh = undefined;
            this.scene = undefined;
        }

    }

}



