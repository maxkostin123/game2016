/**
    * Created by Max Kostin on 26/10/2016.
    */

///<reference path="Snowball.ts" />
///<reference path="Snowman.ts" />
///<reference path="Container.ts" />
///<reference path="ChristmasTree.ts" />
///<reference path="Crane.ts" />
///<reference path="Truck.ts" />

module vt.actors {

    export class MeshFactory {


        public static MESH_CRANE = "crane";
        public static MESH_CONTAINER = "container";
        public static MESH_SNOWMAN = "snowman";
        public static MESH_SNOWBALL = "snowball";
        public static MESH_TRUCK = "truck";
        public static MESH_TREE = "exit";


        public static getActor (mesh:any, type:string, scene:any, settings:any = null): Actor {

            switch (type) {

                case MeshFactory.MESH_CONTAINER :
                    return new Container(mesh, scene);

                case MeshFactory.MESH_CRANE :
                    return new Crane(mesh, scene, settings);

                case MeshFactory.MESH_SNOWMAN :
                    return new Snowman(mesh, scene);

                case MeshFactory.MESH_TRUCK :
                    return new Truck(mesh, scene);

                case MeshFactory.MESH_SNOWBALL :
                    return new Snowball(mesh, scene);

                case MeshFactory.MESH_TREE :
                    return new ChristmasTree(mesh, scene);

            }

            return null;
        }

    }

}

