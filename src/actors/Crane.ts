/**
    * Created by Max Kostin on 26/10/2016.
    */

module vt.actors {


    export class Crane extends Actor {

        protected camera:any;

        private initPos:number = 0;
        private endPos:number = 0;

        private moveVect0 = new BABYLON.Vector3(0,0,-4);

        constructor(mesh:any, scene:any, settings:any) {
            super(mesh, scene, settings);

        }

        public initActor():void {
            this.mesh.position.z -= 6;
            this.initPos = this.mesh.position.z;
            this.endPos = -this.settings.size.height / 5 + 16 + this.initPos;

            T._(this.mesh.position, 20 + Math.floor(Math.random()*5) , [{prop:"z", from: this.initPos, to: this.endPos}], {yoyo:true, repeat:-1, delay:1 + Math.floor(Math.random()*3)});


        }


        public update():void {
            //console.log(this.mesh.intersectsMesh(this.camera.cbox))
            //this.mesh.translate(BABYLON.Axis.Z, -.01, BABYLON.Space.LOCAL);
            //this.mesh.moveWithCollisions(this.moveVect0)
            if (this.mesh.intersectsMesh(this.camera.cbox)) {
                //var v = this.camera.position.add(this.mesh.position).normalize()//.multiply(new BABYLON.Vector3(2, 0, 2));
                //console.log(v)
                //this.camera.position = this.camera.position.subtract(v)
                //this.camera.position.x -= v.x*3;
                //this.camera.position.z -= v.z*3;
            }

        }



    }

}


