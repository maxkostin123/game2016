/**
    * Created by Max Kostin on 26/10/2016.
    */

module vt.actors {


    export class ChristmasTree extends Actor {

        protected camera:any;
        private ps:any;
        private count:number = 0;

        constructor(mesh:any, scene:any) {
            super(mesh, scene);
            this.makeParticles();

        }

        public update():void {

            this.count ++;
            if (this.count > 50) {
                this.count = 0;
                this.twinkle();
            }
        }


        private makeParticles(): void {

            var tree = this.mesh;

            var verts = tree._geometry._meshes[0].getVerticesData(BABYLON.VertexBuffer.PositionKind);
            var len = verts.length;

            var vecsObj:any = {};
            var vecs = [];
            var mult = new BABYLON.Vector3(.1, .1, .1);
            for (var i = 0; i < len; i+=3) {
                var v = new BABYLON.Vector3(verts[i], verts[i+1], verts[i+2]).add(tree.position).clone();
                v.y += .2;
                vecsObj[v.toString()] = v.clone();
            }

            for (var p in vecsObj) {
                vecs.push(vecsObj[p]);
            }


            this.ps = new BABYLON.ParticleSystem("particles", vecs.length, this.scene);
            this.ps.particleTexture = new BABYLON.Texture("assets/star.jpg", this.scene);
            this.ps.manualEmitCount = vecs.length;
            this.ps.color1 = new BABYLON.Color4(0.7, 0.8, 1.0, 1.0);
            this.ps.color2 = new BABYLON.Color4(0.2, 0.5, 1.0, 1.0);
            this.ps.emitter = tree;
            this.ps.minEmitBox = new BABYLON.Vector3(-2, 0, -2);
            this.ps.maxEmitBox = new BABYLON.Vector3(2, 4, 2);
            this.ps.minSize = .7;
            this.ps.maxSize = .7;
            this.ps.minLifeTime = 1000;
            this.ps.maxLifeTime = 1000;
            this.ps.minAngularSpeed = 0;
            this.ps.maxAngularSpeed = 0;
            this.ps.minEmitPower = 0;
            this.ps.maxEmitPower = 0;
            this.ps.updateSpeed = 0.005;
            //this.ps.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;
            this.ps.start();

            setTimeout(()=> {
                for (var i = 0; i < vecs.length; i++) {
                    var p = this.ps.particles[i];
                    p.position = vecs[i];
                    //console.log(p.position)
                    //p.rotation.y = Math.random();
                }
            }, 2500);

        }


        private twinkle():void {
            var len = this.ps.particles.length;
            for (var i = 0; i < len; i++) {
                this.ps.particles[i].color = new BABYLON.Color4(Math.random() * 2, Math.random() * 2, Math.random() * 2, 1);

            }
        }

        private static getRand():number {
            return (Math.random()-Math.random())/3;
        }



    }
}



