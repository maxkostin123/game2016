/**
 * Created by Max Kostin on 25/10/2016
 */


///<reference path="LevelLoader.ts"/>
///<reference path="events/VTEvent.ts" />
///<reference path="GameController.ts" />
///<reference path="SoundManager.ts" />
///<reference path="Service.ts" />
///<reference path="Lists.ts" />

declare var BABYLON: any;


declare var T: {
    _: Function;
    killTweaksOf: Function;
    easeInOut: Function;
    easeIn: Function;
    easeOut: Function;
    linear: Function;
    easeOutBounce: Function;
    easeInOutBack: Function;
    easeOutElastic: Function;
};


module vt {

    import Event = vt.events.VTEvent;
    import VTEvent = vt.events.VTEvent;

    export class Main {

        private assetManager: vt.LevelLoader;
        private gameController: vt.GameController;
        private soundManager: vt.SoundManager;
        private service: vt.Service;

        private overlayDiv: HTMLDivElement;
        private timeHtml: HTMLSpanElement;
        private scoreHtml: HTMLSpanElement;

        private currentLevel = 1;
        private totalLevels = 3;
        private points: number = 100;
        private pointsCounter: number = 100;
        private time = 0;
        private time0 = 0;
        private tTimer = 0;
        private interval = 0;
        private date = new Date();

        private levels = [];
        private score = 0;

        private fieldsData: any[] = [];
        private currentUser: string = ""; // email
        private _isTouch:number = 0;

        private _competionEnded: boolean = false;

        private _canPoll: boolean = true;
        private _isPolling:boolean = false;

        private hud:HTMLDivElement;

        private static level_titles = [
            "TERMINAL START",
            "TERMINAL ARENA",
            "TERMINAL RUN",
        ];


        constructor() {
            this.init();
        }

        /**
         * ========================================================================
         * Init
         * ========================================================================
         */
        private init(): void {

            this.overlayDiv = <HTMLDivElement>document.getElementById("overlay");
            this.overlayDiv.addEventListener("click", (e)=> this.btnClickHandler(e));

            this.timeHtml = <HTMLSpanElement>document.getElementById("disp-time");
            this.scoreHtml = <HTMLSpanElement>document.getElementById("disp-score");

            this.sceneReadyHanlder = this.sceneReadyHanlder.bind(this);
            this.levelCompleteHandler = this.levelCompleteHandler.bind(this);
            this.serverResponseHandler = this.serverResponseHandler.bind(this);
            this.pointsChangeHandler = this.pointsChangeHandler.bind(this);
            this.updateTime = this.updateTime.bind(this);

            this.assetManager = new vt.LevelLoader();
            this.gameController = new vt.GameController();
            this.soundManager = vt.SoundManager.getInstance();
            this.service = vt.Service.getInstance();

            this.assetManager.addListener(Event.SCENE_READY, this.sceneReadyHanlder);
            this.gameController.addListener(Event.LEVEL_COMPLETE, this.levelCompleteHandler);
            this.gameController.addListener(Event.POINTS_CHANGE, this.pointsChangeHandler);
            this.service.addListener(Event.SERVER_RESULT, this.serverResponseHandler);


            var preloader = document.getElementById("preloader");
            T._(preloader, .5, [{prop: "scaleY", to: 0}, {prop: "scaleZ", to: 1}], {delay: 1}, {call:()=> {
                preloader.parentNode.removeChild(preloader);
            }});

            this.timeHtml.innerHTML = "00:00";
            this.scoreHtml.innerHTML = this.points + "";

            this.touchTest = this.touchTest.bind(this);
            window.addEventListener('touchstart', this.touchTest);

            document.getElementById("twit-start").setAttribute("href", "https://twitter.com/share?url=https://game2016.videotel-cloud.com&hashtags=videotel,game2016&text=Videotel's Christmas Game&source=webclient");
            document.getElementById("fb-start").setAttribute("href", "https://www.facebook.com/sharer/sharer.php?u=https://game2016.videotel-cloud.com");

            this.hud = <HTMLDivElement>document.getElementById("hud");

            Main.makeCountryList();

            //this.setScreen("sendScore");
            //this.setScreen("gameComplete");
            //this.setScreen("scoreboard");


        }

        private touchTest():void {
            this._isTouch = 1;
            this.gameController.isTouchDevice = true;
            window.removeEventListener('touchstart', this.touchTest);
            document.getElementById("controls").classList.remove("hidden");
        }


        private sceneReadyHanlder(e: Event): void {
            this.hud.classList.remove("hidden");
            this.gameController.initLevel(e.data.scene, e.data.engine, e.data.data);
            this.soundManager.init(e.data.scene);
            this.time0 = Date.now();
            this.interval = 0;
            this.tTimer = requestAnimationFrame(this.updateTime);

            T._(this.overlayDiv, .5, [{prop: "translateY", from: "0vh", to: "-100vh"}, {prop: "scaleZ", to: 1}], {delay: 1});

        }

        private pointsChangeHandler(e): void {

            this.points += e.data;
            this.updateStatus();


            if (this.points <= 0) {
                this.points = 0;
                this.gameController.pause();
                document.querySelector("#gameFail span[data-id='time']").innerHTML = this.getTimeStr();
                document.querySelector("#gameFail span[data-id='score']").innerHTML = this.points + "";
                this.setScreen("gameFail");
                this.soundManager.dispose();
                this.showOverlay();
            }

        }

        private updateStatus(): void {

            T._(this, .5, [{prop: "pointsCounter", to: this.points}], {
                onUpdate: ()=> {
                    this.scoreHtml.innerHTML = Math.ceil(this.pointsCounter) + "";
                    if (this.points <= 80 && this.points > 50) {
                        this.scoreHtml.classList.add("warning");
                    } else if (this.points <= 50) {
                        this.scoreHtml.classList.add("danger");
                    } else {
                        this.scoreHtml.classList.remove("danger");
                        this.scoreHtml.classList.remove("warning");
                    }
                }
            })
        }

        private updateTime(): void {

            this.interval++;
            if (this.interval === 60) {

                this.interval = 0;
                this.time = Date.now() - this.time0;
                this.timeHtml.innerHTML = this.getTimeStr();
            }

            this.tTimer = requestAnimationFrame(this.updateTime);
        }


        private getTimeStr(): string {
            this.date.setTime(this.time);
            var mins = this.date.getMinutes();
            var secs = this.date.getSeconds();

            var mStr = mins > 9 ? mins + "" : "0" + mins;
            var sStr = secs > 9 ? secs + "" : "0" + secs;

            return mStr + ":" + sStr;
        }


        /**
         * ========================================================================
         * Level complete
         * @param e
         * ========================================================================
         */
        private levelCompleteHandler(e: Event): void {
            this.hud.classList.add("hidden");
            this.gameController.pause();
            this.soundManager.dispose();
            if (this.currentLevel < this.totalLevels) {
                this.levels.push({points: this.points, time: this.time});
                document.querySelector("#levelComplete span[data-id='time']").innerHTML = this.getTimeStr();
                document.querySelector("#levelComplete span[data-id='score']").innerHTML = this.points + "";
                this.setScreen("levelComplete");
                document.querySelector(`span[data-id="l-${this.currentLevel}"]`).classList.add("l-complete");
            } else {
                this.levels.push({points: this.points, time: this.time});
                this.setScore();
                document.querySelector("#gameComplete span[data-id='score']").innerHTML = this.score + "";
                this.setScreen("gameComplete");
            }
            this.showOverlay();

        }


        private setScore() {
            var time: number = 0;
            var points = 0;

            for (var i = 0; i < this.levels.length; i++) {
                time += this.levels[i].time;
                points += this.levels[i].points;
            }

            this.score = Math.round(points / (time/2000) * 300);

        }

        /**
         * ========================================================================
         * UI clicks handler
         * @param e
         * ========================================================================
         */
        private btnClickHandler(e): void {
            var targ = e.target;
            var id = targ.getAttribute("data-id");

            if (!id || targ.getAttribute("data-disabled")) {
                return;
            }
            this.points = 100;
            this.updateStatus();
            this.gameController.destroy();
            document.getElementById("overlay").scrollTop = 0;

            switch (id) {

                case "start" :
                    this.setScreen("loader");
                    this.assetManager.loadScene("shipyard.json", "assets/level_1.json");
                    document.body.scrollTop = 0;
                    break;

                case "next" :
                    this.gameController.destroy();
                    if (this.currentLevel < this.totalLevels) {
                        this.setScreen("loader");
                        this.currentLevel++;
                        this.assetManager.loadScene("shipyard.json", "assets/level_" + this.currentLevel + ".json");
                    }
                    break;

                case "replay" :
                    window.location.reload();
                    break;

                case "fill-form" :
                    this.setScore();
                    this.setScreen("sendScore");
                    this.prefillForm();
                    break;

                case "send-score" :
                    document.querySelector("#scoreboard button[data-id='replay']").classList.remove("hidden");
                    document.querySelector("#scoreboard button[data-id='back']").classList.add("hidden");
                    this._isPolling = false;
                    this.sendScore();
                    break;


                case "help" :
                    this.setScreen("help");
                    break;

                case "back" :
                    this.setScreen("gameStart");
                    this._canPoll = false;
                    break;

                case "t-a-c" :
                    this.setScreen("tac");
                    break;

                case "back-2-form" :
                    this.setScreen("sendScore");
                    this._canPoll = true;
                    break;

                case "show-scores" :
                    this.service.getScores();
                    this._canPoll = true;
                    this._isPolling = false;
                    this.prefillForm();
                    document.querySelector("#scoreboard button[data-id='replay']").classList.add("hidden");
                    document.querySelector("#scoreboard button[data-id='back']").classList.remove("hidden");
                    break;


            }
            var title: string = Main.level_titles[this.currentLevel - 1];
            document.getElementById("level-title").innerHTML = title;
            document.title = title;
        }


        private setScreen(screen: string): void {
            var divs = this.overlayDiv.querySelectorAll(".res");

            for (var i = 0; i < divs.length; i++) {
                divs[i].classList.add("hidden");
            }
            this.overlayDiv.querySelector("#" + screen).classList.remove("hidden");
        }

        private showOverlay(): void {
            T._(this.overlayDiv, .5, [{prop: "translateY", to: "0vh"}, {prop: "scaleZ", to: 1}], {delay: 1});
            window.cancelAnimationFrame(this.tTimer);
        }


        /**
         * ========================================================================
         * Send score
         * ========================================================================
         */
        private sendScore(): void {

            var fname = <HTMLInputElement>document.querySelector("input[data-id='fname']");
            var femail = <HTMLInputElement>document.querySelector("input[data-id='email']");
            var tick = <HTMLInputElement>document.querySelector("input[data-id='tc-check']");
            var ok = true;

            fname.classList.remove("invalid");
            femail.classList.remove("invalid");
            document.querySelector(".agreement").classList.remove("invalid");

            femail.value = femail.value.toLowerCase();
            fname.value = fname.value.trim();

            if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/.test(femail.value)) {
                femail.classList.add("invalid");
                ok = false;
            }

            if (fname.value.length < 2 || ! vt.Lists.check(fname.value)) {
                fname.classList.add("invalid");
                ok = false;
            }


            if (! tick.checked) {
               document.querySelector(".agreement").classList.add("invalid");
                ok = false;
            }

            if (ok) {
                this.currentUser = femail.value;
                var str = "";
                this.fieldsData = [];
                var fields = document.getElementById("sendScore").querySelectorAll("input[type=text], input[type=email]");
                var id;
                for (var i = 0; i < fields.length; i++) {
                    var f = <HTMLInputElement>fields[i];
                    var name = f.getAttribute("data-id");
                    var value = f.value;
                    value = value.replace(/[^\w\W.@\s\-_&]/gmiu, "");
                    value = value.replace(/['"]/g, "");
                    value = encodeURIComponent(value);
                    str += `'${value}',`;
                    if (name === "email") {
                        id = value;
                    }
                    this.fieldsData.push({field:name, value:value});
                }

                var sel = <HTMLSelectElement>document.querySelector("select[data-id='country']");
                var country = sel.selectedOptions[0].value;
                this.fieldsData.push({field:"country", value:sel.selectedIndex});

                str += this.score;
                str += ", " + this._isTouch;
                str += ", '01010001100'";
                str += `, '${encodeURIComponent(country)}'`;

                var query = `INSERT INTO t1 (fname, email, company, vessel, rank, score, touchDevice, ip, country) values (${str})`;
                //console.log(query)
                var btn = document.querySelector("button[data-id='send-score']");
                btn.setAttribute("data-disabled", "true");
                btn.classList.add("disabled");
                this.service.setLocalData(this.fieldsData);
                this.service.sendScore(query);
                this.setSocialLinks();
                this._canPoll = true;
            }
        }

        /**
         * ========================================================================
         * Server response handler
         * @param e
         * ========================================================================
         */
        private serverResponseHandler(e: VTEvent): void {

            switch (e.data.type) {

                case VTEvent.SERVER_COMPETITION_EXPIRED:
                    this._competionEnded = true;
                    var btn = document.querySelector("button[data-id='fill-form']");
                    btn.setAttribute("data-disabled", "true");
                    btn.classList.add("disabled");
                    document.querySelector(".competition-ended").classList.remove("hidden");
                    break;

                case VTEvent.SERVER_SCORE_SENT:
                    this.service.getScores();
                    break;


                case VTEvent.SERVER_COMPETITION_RESULT:
                    var res: any[] = e.data.result;
                    var div = document.getElementById("game-result");
                    var str = "<div class='res-row'><span>Rank</span><span>Name</span><span>Best Score</span></div>";

                    for (var i = 0; i < res.length; i++) {
                        var field = res[i];
                        var userHere = (field.email === this.currentUser) ? "user" : "";
                        str += `<div class="res-row ${userHere}"><span>${i + 1}</span><span>${field.name}</span><span>${field.score}</span></div>`;
                    }
                    div.innerHTML = str;
                    if (! this._isPolling) {
                        T._(this.overlayDiv, .5, [{prop: "translateY", to: "-100vh"}], {
                            call: ()=> {
                                this.setScreen("scoreboard");
                                this.showOverlay();
                                this._isPolling = true;
                            }
                        });
                    }
                    // Poll results ---
                    if (this._canPoll) {
                        setTimeout(() => {
                            this.service.getScores();
                            console.log("--polling");
                        }, 5000);
                    }
                    break;

            }
        }

        private setSocialLinks():void {
            var shareTxt = encodeURIComponent(`I've just played Terminal Run! My score is ${this.score}`);
            document.getElementById("twit-end")["href"] = `https://twitter.com/share?url=https://game2016.videotel-cloud.com&hashtags=videotel,game2016&text=I've just played Terminal Run! My score is ${this.score}&source=webclient`;
            document.getElementById("fb-end").setAttribute("href", "https://www.facebook.com/sharer/sharer.php?u=https://game2016.videotel-cloud.com");
        }

        private prefillForm():void {
            var ld:any[] = this.service.getLocalData();
            if (ld) {
                for (var i = 0; i < ld.length; i++) {
                    var o = ld[i];
                    var value = decodeURIComponent(o.value);
                    if (o.field === "email") {
                        this.currentUser = value;
                    }
                    if (o.field !== "country"){
                        document.querySelector(`input[data-id=${o.field}]`)["value"] = value;
                    } else {
                        var sel = <HTMLSelectElement>document.querySelector("select[data-id='country']");
                        sel.selectedIndex = o.country;
                        sel.options[o.value].setAttribute("selected", "");
                    }
                }

            }

        }

        private static makeCountryList():void {

            var l = vt.Lists.COUNTRY_LIST;
            var str = `<option value=""></option>`;
            for (var i = 0; i < l.length; i++) {
                var c = l[i];
                str += `<option value="${c}">${c}</option>`
            }

            document.querySelector("select[data-id='country']").innerHTML = str;

        }


    }
}







