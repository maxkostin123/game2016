/**
 * Created by Max Kostin on 23/11/2016.
 */

///<reference path="events/VTEvent.ts" />

module vt {

    import VTEvent = vt.events.VTEvent;

    export class Service extends vt.events.Dispatcher {

        public static LOCAL_ID = "__VT_GAME_2016__";

        private static __instance: Service;
        private localStorageAvailable:boolean;

        constructor(enforcer:Enforcer2) {
            super();
            this.localStorageAvailable = Service.checkStorageAvailable();
            // Check if competition is still running
            this.sendQuery("func=checkStatus");
        }





        public static getInstance():Service {

            if (! Service.__instance) {
                Service.__instance = new Service(new Enforcer2());
            }
            return Service.__instance;
        }

        public getScores():void {
            this.sendQuery("func=getResults");
        }

        public sendScore(query:string):void {
            this.sendQuery("func=sendScore&query="+query);
        }


        private sendQuery(query:string):void {

            var xhr = new XMLHttpRequest();
            xhr.addEventListener("load", (evt)=> this.xhrResponseHandler(evt));
            xhr.addEventListener("error", (evt)=> this.xhrResponseHandler(evt));

            xhr.open("POST", "api/");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xhr.send(query);
        }

        private xhrResponseHandler(evt) {

            //console.log(evt.target.responseText);

            if(evt.target.responseText.indexOf("Cannot POST") > -1){
                this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, {type:VTEvent.SERVER_NO_CONN}));
                return;
            }

            var res;
            try {
               res = JSON.parse(evt.target.responseText);
            } catch (er) {
                console.log(er);
                return;
            }

            switch (res.type) {

                case "competitionExpired" :
                    if (res.response === true) {
                        this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, {type:VTEvent.SERVER_COMPETITION_EXPIRED}));
                    }
                    break;

                case "dbInsert" :
                    this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, {type:VTEvent.SERVER_SCORE_SENT}));
                    break;

                case "dbResult" :
                    this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, {type:VTEvent.SERVER_COMPETITION_RESULT, result:res.response}));
                    break;

                case "error" :
                    console.log("Error:", res.response);
                    break;
            }

        }

        private xhrErrorHandler(evt) {
            console.log(evt.target);
        }


        public setLocalData(data:any):void {
            if (this.localStorageAvailable) {
                window.localStorage.setItem(Service.LOCAL_ID, JSON.stringify(data));
            }
        }

        public getLocalData():any[] {
            if (this.localStorageAvailable) {
                return JSON.parse(window.localStorage.getItem(Service.LOCAL_ID));
            }
            return null;
        }

        private static checkStorageAvailable():boolean {
            try {
                var storage = window.localStorage,
                    x = '__storage_test__';
                storage.setItem(x, x);
                storage.removeItem(x);
                return true;
            }
            catch(e) {
                console.log("localStorage is unavailable.");
                return false;
            }
        }



    }
}
class Enforcer2{}











