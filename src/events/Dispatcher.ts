/**
 * Created by max.kostin on 30/01/2015
 */

///<reference path = "VTEvent.ts"/>

module vt.events
{

	import VTEvent = vt.events.VTEvent;

	/**
	 * Provides basic Observer/Notifier functionality
	 */
	export class Dispatcher
	{
		private _listeners:Object = {};

		public addListener(type:string, listener:Function):void
		{
			if(this._listeners[type] === undefined)
			{
				this._listeners[type] = [];
			}
			this._listeners[type].push(listener);
		}

		/**
		 * Removes a listener
		 * @param type Event type
		 * @param listener listener function
		 */
		public removeListener(type:string, listener:Function):void
		{
			if (! this._listeners[type]) return;
			var ind = this._listeners[type].indexOf(listener);
			if (ind != -1) this._listeners[type].splice(ind, 1);
		}

		/**
		 * Removes all listeners of this instance. If type param is provided, only listeners of that particular type
		 * will be removed.
		 * @param type event type to remove
		 */
		public removeAllListeners(type?: string):void
		{
			if (type){
				delete this._listeners[type];
			} else {
				this._listeners = {};
			}
		}

		/**
		 * Checks if a listener of a particular type exists
		 * @param type
		 * @param listener
		 * @returns {boolean}
		 */
		public hasListener(type:string, listener:Function):boolean
		{
			return (this._listeners[type] !== undefined && this._listeners[type].indexOf(listener) != -1);
		}

		/**
		 * Dispatches Event
		 * @param evt
		 */
		public dispatch(evt:VTEvent):void
		{
			if (this._listeners[evt.type])
			{
				evt.target = this;
				this._listeners[evt.type].forEach((el) => {
					el.call(this, evt);
				});
			}
		}

	}
}
