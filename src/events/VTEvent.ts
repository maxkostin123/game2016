/**
 * Created by max.kostin on 28/01/2015
 */
module vt.events {

    export class VTEvent {

        public static SCENE_READY = "evt.scene.ready";
        public static LEVEL_COMPLETE = "evt.level.complete";
        public static POINTS_CHANGE = "evt.game.pointsChange";
        public static SERVER_RESULT = "evt.serverResult";

        public static SERVER_NO_CONN = "evt.server.noconn";
        public static SERVER_COMPETITION_EXPIRED = "evt.server.competitionExpired";
        public static SERVER_SCORE_SENT = "evt.server.scoreSent";
        public static SERVER_COMPETITION_RESULT = "evt.server.competitionResult";

        constructor(public type:string, public data:any = null, public target:any = null) {

        }

        public clone():VTEvent {
            return new VTEvent(this.type, this.data, this.target);
        }

    }

}