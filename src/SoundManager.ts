/**
 * Created by vthome on 18/11/2016.
 */

module vt {


    export class SoundManager {


        private static __instance: SoundManager;
        private scene:any;
        private snowmenSounds = [];
        private soundsCounter = 0;

       private sounds:any = {};


        constructor(enforcer:Enforcer) {

        }


        public static getInstance():SoundManager {
            if (! SoundManager.__instance) SoundManager.__instance = new SoundManager(new Enforcer());
            return this.__instance;
        }


        public init(scene:any):void {

            this.scene = scene;
            this.dispose();
            this.sounds.close_to_exit = new BABYLON.Sound("close_to_exit", "assets/audio/jbells.mp3", this.scene, null, { loop: true, autoplay: true, spatialSound: true, maxDistance: 90});
            this.sounds.ambient = new BABYLON.Sound("ambient", "assets/audio/terminal.mp3", this.scene, null, { loop: true, autoplay: true });
            this.sounds.player_hit = new BABYLON.Sound("player_hit", "assets/audio/player_hit.mp3", this.scene, null, { loop: false, autoplay: false});
            this.sounds.player_walk = new BABYLON.Sound("player_walk", "assets/audio/walk2.mp3", this.scene, null, { loop: true, autoplay: false });
            this.sounds.crane = new BABYLON.Sound("crane", "assets/audio/crane.mp3", this.scene, null, { loop: true, autoplay: false, spatialSound: true, maxDistance: 90 });
            this.sounds.ball_throw = new BABYLON.Sound("ball_throw", "assets/audio/ball_throw.mp3", this.scene, null, { loop: false, autoplay: false });
            this.sounds.snowman_hit = new BABYLON.Sound("snowman_hit", "assets/audio/grunt1.mp3", this.scene, null, { loop: false, autoplay: false, spatialSound: true, maxDistance: 70});
            this.sounds.snowman_throw = new BABYLON.Sound("snowman_throw", "assets/audio/hey.mp3", this.scene, null, { loop: false, autoplay: false, spatialSound: true, maxDistance: 90});
            this.sounds.present_pick = new BABYLON.Sound("pick_present", "assets/audio/ding1.mp3", this.scene, null, { loop: false, autoplay: false});
        }

        public playCloseToExit(vector:any):void {

            this.sounds.close_to_exit.setPosition(vector);
            //this.sounds.close_to_exit.play();
        }

        public playerHit():void {
            this.sounds.player_hit.play();
        }

        public pickPresent():void {
            this.sounds.present_pick.play();
        }


        public playerWalk():void {
            this.sounds.player_walk.play();
        }

        public stopPlayerWalk():void {
            this.sounds.player_walk.stop();
        }

        public throwBall():void {
            this.sounds.ball_throw.play();
        }

        public addSnowmanSound(pos):void {
            this.sounds["snowman_hit"] = new BABYLON.Sound("snowman_hit", "assets/audio/grunt1.mp3", this.scene, null, { loop: false, autoplay: false, spatialSound: true, maxDistance: 70});
        }

        public snowmanHit(mesh:any):void {
            this.sounds.snowman_hit.setPosition(mesh.pos);
            this.sounds.snowman_hit.play();
        }

        public snowmanThrow(mesh:any):void {
            //this.sounds.snowman_throw.attachToMesh(mesh);
            //this.sounds.snowman_throw.play();
        }


        public playCrane(mesh:any):void {
            this.sounds.crane.attachToMesh(mesh);
            this.sounds.crane.play();
        }


        public makeAnouncer(pos:any, makeRefBox:boolean=false):void {
            setTimeout(()=> {
                var vect = new BABYLON.Vector3(pos.x, 2, pos.z);
                this.sounds["announcer"+this.soundsCounter]  = new BABYLON.Sound("s"+this.soundsCounter, "assets/audio/anounce.mp3", this.scene, null,
                    { loop: true, autoplay: true, spatialSound: true, maxDistance: 90});
                this.sounds["announcer"+this.soundsCounter].setPosition(vect);
                if (makeRefBox) {
                    var b = BABYLON.Mesh.CreateBox("box", 6.0, this.scene);
                    b.position = vect;
                }
            }, 500);
            this.soundsCounter++;
        }


        public dispose():void {
            for (var p in this.sounds) {
                if (this.sounds.hasOwnProperty(p)) {
                    this.sounds[p].dispose();
                }
            }
        }


    }




}

class Enforcer{}
