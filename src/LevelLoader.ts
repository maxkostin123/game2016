/**
 * Created by Max Kostin on 25/10/2016
 */

///<reference path="events/Dispatcher.ts" />
///<reference path="events/VTEvent.ts" />

module vt {

    import Dispatcher = vt.events.Dispatcher;
    import Event = vt.events.VTEvent;

    export class LevelLoader extends vt.events.Dispatcher {

        private canvas: HTMLCanvasElement;
        private engine: any;
        private scene;
        private level:string;


        constructor() {
            super();
        }

        public loadScene(scene: string, level: string): void {

            this.level = level;

            if (BABYLON.Engine.isSupported()) {

                this.canvas = <HTMLCanvasElement>document.getElementById("canvas");
                if (! this.engine) this.engine = new BABYLON.Engine(this.canvas, true, {stencil:true}, false); // canvas, antialiasing, stensil, AdaptToDeviceRatio


                BABYLON.SceneLoader.Load("assets/", scene, this.engine, (scene)=> {
                    this.sceneReady(scene);
                });
            } else {
                alert("Error: Your browser does not support WebGL");
            }

        }


        private sceneReady(scene: any) {

            this.scene = scene;


            scene.executeWhenReady(()=> {

                document.body.setAttribute("oncontextmenu", "return false"); // Disable right-mouse.
                this.loadLevel();

            });

        }

        private loadLevel(): void {
            var that = this;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", this.level, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
            xhr.onload = function () {
                var json = JSON.parse(this.responseText);
                that.dispatch(new Event(Event.SCENE_READY, {scene: that.scene, engine:that.engine, data:json}, this));

            };
            xhr.send();
        }



    }




}