/**
    * Created by Max Kostin on 25/10/2016.
    */

module vt {

    export interface IMap<T> {
        [index: string]: T;
        [index: number]: T;
    }

    export interface IActor {
        type: string;
        mesh: any;
        settings: any;
        name: string;
        checkCollisions: boolean;
        clone: boolean;
    }




}