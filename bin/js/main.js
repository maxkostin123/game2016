var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var vt;
(function (vt) {
    var events;
    (function (events) {
        var VTEvent = (function () {
            function VTEvent(type, data, target) {
                if (data === void 0) { data = null; }
                if (target === void 0) { target = null; }
                this.type = type;
                this.data = data;
                this.target = target;
            }
            VTEvent.prototype.clone = function () {
                return new VTEvent(this.type, this.data, this.target);
            };
            VTEvent.SCENE_READY = "evt.scene.ready";
            VTEvent.LEVEL_COMPLETE = "evt.level.complete";
            VTEvent.POINTS_CHANGE = "evt.game.pointsChange";
            VTEvent.SERVER_RESULT = "evt.serverResult";
            VTEvent.SERVER_NO_CONN = "evt.server.noconn";
            VTEvent.SERVER_COMPETITION_EXPIRED = "evt.server.competitionExpired";
            VTEvent.SERVER_SCORE_SENT = "evt.server.scoreSent";
            VTEvent.SERVER_COMPETITION_RESULT = "evt.server.competitionResult";
            return VTEvent;
        }());
        events.VTEvent = VTEvent;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var events;
    (function (events) {
        var Dispatcher = (function () {
            function Dispatcher() {
                this._listeners = {};
            }
            Dispatcher.prototype.addListener = function (type, listener) {
                if (this._listeners[type] === undefined) {
                    this._listeners[type] = [];
                }
                this._listeners[type].push(listener);
            };
            Dispatcher.prototype.removeListener = function (type, listener) {
                if (!this._listeners[type])
                    return;
                var ind = this._listeners[type].indexOf(listener);
                if (ind != -1)
                    this._listeners[type].splice(ind, 1);
            };
            Dispatcher.prototype.removeAllListeners = function (type) {
                if (type) {
                    delete this._listeners[type];
                }
                else {
                    this._listeners = {};
                }
            };
            Dispatcher.prototype.hasListener = function (type, listener) {
                return (this._listeners[type] !== undefined && this._listeners[type].indexOf(listener) != -1);
            };
            Dispatcher.prototype.dispatch = function (evt) {
                var _this = this;
                if (this._listeners[evt.type]) {
                    evt.target = this;
                    this._listeners[evt.type].forEach(function (el) {
                        el.call(_this, evt);
                    });
                }
            };
            return Dispatcher;
        }());
        events.Dispatcher = Dispatcher;
    })(events = vt.events || (vt.events = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var Event = vt.events.VTEvent;
    var LevelLoader = (function (_super) {
        __extends(LevelLoader, _super);
        function LevelLoader() {
            _super.call(this);
        }
        LevelLoader.prototype.loadScene = function (scene, level) {
            var _this = this;
            this.level = level;
            if (BABYLON.Engine.isSupported()) {
                this.canvas = document.getElementById("canvas");
                if (!this.engine)
                    this.engine = new BABYLON.Engine(this.canvas, true, { stencil: true }, false);
                BABYLON.SceneLoader.Load("assets/", scene, this.engine, function (scene) {
                    _this.sceneReady(scene);
                });
            }
            else {
                alert("Error: Your browser does not support WebGL");
            }
        };
        LevelLoader.prototype.sceneReady = function (scene) {
            var _this = this;
            this.scene = scene;
            scene.executeWhenReady(function () {
                document.body.setAttribute("oncontextmenu", "return false");
                _this.loadLevel();
            });
        };
        LevelLoader.prototype.loadLevel = function () {
            var that = this;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", this.level, true);
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
            xhr.onload = function () {
                var json = JSON.parse(this.responseText);
                that.dispatch(new Event(Event.SCENE_READY, { scene: that.scene, engine: that.engine, data: json }, this));
            };
            xhr.send();
        };
        return LevelLoader;
    }(vt.events.Dispatcher));
    vt.LevelLoader = LevelLoader;
})(vt || (vt = {}));
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var Actor = (function (_super) {
            __extends(Actor, _super);
            function Actor(mesh, scene, settings) {
                if (settings === void 0) { settings = null; }
                _super.call(this);
                this.scene = scene;
                this.settings = settings;
                this.mesh = mesh;
                this.camera = scene.activeCamera;
            }
            Actor.prototype.update = function () {
            };
            Actor.prototype.initActor = function () {
            };
            Actor.prototype.hit = function () {
            };
            Actor.prototype.destroy = function () {
                this.camera = undefined;
                this.mesh = undefined;
                this.scene = undefined;
            };
            return Actor;
        }(vt.events.Dispatcher));
        actors.Actor = Actor;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var Snowball = (function (_super) {
            __extends(Snowball, _super);
            function Snowball(mesh, scene) {
                _super.call(this, mesh, scene);
            }
            Snowball.prototype.update = function () {
                this.mesh.translate(BABYLON.Axis.Z, -.1, BABYLON.Space.LOCAL);
            };
            return Snowball;
        }(actors.Actor));
        actors.Snowball = Snowball;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var SoundManager = (function () {
        function SoundManager(enforcer) {
            this.snowmenSounds = [];
            this.soundsCounter = 0;
            this.sounds = {};
        }
        SoundManager.getInstance = function () {
            if (!SoundManager.__instance)
                SoundManager.__instance = new SoundManager(new Enforcer());
            return this.__instance;
        };
        SoundManager.prototype.init = function (scene) {
            this.scene = scene;
            this.dispose();
            this.sounds.close_to_exit = new BABYLON.Sound("close_to_exit", "assets/audio/jbells.mp3", this.scene, null, { loop: true, autoplay: true, spatialSound: true, maxDistance: 90 });
            this.sounds.ambient = new BABYLON.Sound("ambient", "assets/audio/terminal.mp3", this.scene, null, { loop: true, autoplay: true });
            this.sounds.player_hit = new BABYLON.Sound("player_hit", "assets/audio/player_hit.mp3", this.scene, null, { loop: false, autoplay: false });
            this.sounds.player_walk = new BABYLON.Sound("player_walk", "assets/audio/walk2.mp3", this.scene, null, { loop: true, autoplay: false });
            this.sounds.crane = new BABYLON.Sound("crane", "assets/audio/crane.mp3", this.scene, null, { loop: true, autoplay: false, spatialSound: true, maxDistance: 90 });
            this.sounds.ball_throw = new BABYLON.Sound("ball_throw", "assets/audio/ball_throw.mp3", this.scene, null, { loop: false, autoplay: false });
            this.sounds.snowman_hit = new BABYLON.Sound("snowman_hit", "assets/audio/grunt1.mp3", this.scene, null, { loop: false, autoplay: false, spatialSound: true, maxDistance: 70 });
            this.sounds.snowman_throw = new BABYLON.Sound("snowman_throw", "assets/audio/hey.mp3", this.scene, null, { loop: false, autoplay: false, spatialSound: true, maxDistance: 90 });
            this.sounds.present_pick = new BABYLON.Sound("pick_present", "assets/audio/ding1.mp3", this.scene, null, { loop: false, autoplay: false });
        };
        SoundManager.prototype.playCloseToExit = function (vector) {
            this.sounds.close_to_exit.setPosition(vector);
        };
        SoundManager.prototype.playerHit = function () {
            this.sounds.player_hit.play();
        };
        SoundManager.prototype.pickPresent = function () {
            this.sounds.present_pick.play();
        };
        SoundManager.prototype.playerWalk = function () {
            this.sounds.player_walk.play();
        };
        SoundManager.prototype.stopPlayerWalk = function () {
            this.sounds.player_walk.stop();
        };
        SoundManager.prototype.throwBall = function () {
            this.sounds.ball_throw.play();
        };
        SoundManager.prototype.addSnowmanSound = function (pos) {
            this.sounds["snowman_hit"] = new BABYLON.Sound("snowman_hit", "assets/audio/grunt1.mp3", this.scene, null, { loop: false, autoplay: false, spatialSound: true, maxDistance: 70 });
        };
        SoundManager.prototype.snowmanHit = function (mesh) {
            this.sounds.snowman_hit.setPosition(mesh.pos);
            this.sounds.snowman_hit.play();
        };
        SoundManager.prototype.snowmanThrow = function (mesh) {
        };
        SoundManager.prototype.playCrane = function (mesh) {
            this.sounds.crane.attachToMesh(mesh);
            this.sounds.crane.play();
        };
        SoundManager.prototype.makeAnouncer = function (pos, makeRefBox) {
            var _this = this;
            if (makeRefBox === void 0) { makeRefBox = false; }
            setTimeout(function () {
                var vect = new BABYLON.Vector3(pos.x, 2, pos.z);
                _this.sounds["announcer" + _this.soundsCounter] = new BABYLON.Sound("s" + _this.soundsCounter, "assets/audio/anounce.mp3", _this.scene, null, { loop: true, autoplay: true, spatialSound: true, maxDistance: 90 });
                _this.sounds["announcer" + _this.soundsCounter].setPosition(vect);
                if (makeRefBox) {
                    var b = BABYLON.Mesh.CreateBox("box", 6.0, _this.scene);
                    b.position = vect;
                }
            }, 500);
            this.soundsCounter++;
        };
        SoundManager.prototype.dispose = function () {
            for (var p in this.sounds) {
                if (this.sounds.hasOwnProperty(p)) {
                    this.sounds[p].dispose();
                }
            }
        };
        return SoundManager;
    }());
    vt.SoundManager = SoundManager;
})(vt || (vt = {}));
var Enforcer = (function () {
    function Enforcer() {
    }
    return Enforcer;
}());
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var Snowman = (function (_super) {
            __extends(Snowman, _super);
            function Snowman(mesh, scene) {
                _super.call(this, mesh, scene);
                this._health = 9;
                this.balls = [];
                this.canThrow = true;
                this._isAlive = true;
                this.init();
            }
            Snowman.prototype.init = function () {
                this.collisionActors = vt.GameController.getInstance().getCollisionActors();
                this.mesh.checkCollisions = false;
                this.scene.beginAnimation(this.mesh.skeleton, 0, 1, 1.0, false);
                this.bone = this.scene.getBoneByName("bone-rHand");
                this.mesh.computeBonesUsingShaders = !vt.GameController.getInstance().isTouchDevice;
                this.mesh.lookAt(this.camera.position);
                this.pose = this.mesh.position.clone();
                this.pose.y = 1;
                var rot = this.mesh._rotationQuaternion.toEulerAngles().clone();
                rot.x = 0;
                this.ray = new BABYLON.Ray(this.pose, this.mesh.getDirection(new BABYLON.Vector3(0, 0, -1)), 90);
                for (var i = 0; i < this.collisionActors.length; i++) {
                    var box = this.collisionActors[i].mesh._boundingInfo.boundingBox;
                }
            };
            Snowman.prototype.scan = function () {
                var box = this.camera.cbox;
                return this.mesh.getDistanceToCamera(this.camera) < 100;
            };
            Snowman.prototype.destroy = function () {
                _super.prototype.destroy.call(this);
                for (var i = 0; i < this.balls.length; i++) {
                    this.balls[i].dispose();
                }
            };
            Snowman.prototype.hit = function () {
                this._health -= 3;
                this.makeSnow();
                if (this._health <= 0) {
                    this.makeSnow(500, -2);
                    this._isAlive = false;
                    this.mesh.rotation.z = Math.PI / 2;
                    vt.GameController.getInstance().playerAddPoints(500);
                }
                else {
                    this.makeSnow(30);
                }
            };
            Snowman.prototype.makeSnow = function (numParticles, gravity) {
                if (numParticles === void 0) { numParticles = 80; }
                if (gravity === void 0) { gravity = -10; }
                var particleSystem = new BABYLON.ParticleSystem("particles", 200, this.scene);
                particleSystem.particleTexture = new BABYLON.Texture("assets/snowflake.png", this.scene);
                particleSystem.minEmitBox = new BABYLON.Vector3(-.5, .5, -.5);
                particleSystem.gravity = new BABYLON.Vector3(0, gravity, 0);
                particleSystem.color1 = new BABYLON.Color4(0.7, 0.8, 1.0, 1.0);
                particleSystem.color2 = new BABYLON.Color4(0.2, 0.5, 1.0, 1.0);
                particleSystem.emitter = this.mesh;
                particleSystem.manualEmitCount = numParticles;
                particleSystem.maxEmitBox = new BABYLON.Vector3(.5, 1, .5);
                particleSystem.minSize = 0.1;
                particleSystem.maxSize = 0.3;
                particleSystem.minLifeTime = 1;
                particleSystem.maxLifeTime = 2;
                particleSystem.direction1 = new BABYLON.Vector3(-2, 2, -2);
                particleSystem.direction2 = new BABYLON.Vector3(5, 5, 5);
                particleSystem.minAngularSpeed = -Math.PI;
                particleSystem.maxAngularSpeed = Math.PI;
                particleSystem.disposeOnStop = true;
                particleSystem.start();
            };
            Snowman.prototype.update = function () {
                var _this = this;
                if (this._isAlive) {
                    this.mesh.lookAt(this.camera.position.clone());
                }
                for (var i = this.balls.length - 1; i >= 0; i--) {
                    var b = this.balls[i];
                    b.time++;
                    b.translate(BABYLON.Axis.Z, -b.speed, BABYLON.Space.LOCAL);
                    if (b.time > 200) {
                        this.removeBall(b);
                    }
                    if (b.intersectsMesh(this.camera.cbox)) {
                        vt.SoundManager.getInstance().playerHit();
                        this.removeBall(b);
                        vt.GameController.getInstance().playerAddPoints(-30);
                        T._(this.camera.rotation, .09, [{ prop: "x", to: -20 * Math.PI / 180 }], { yoyo: true, repeat: 1, call: function () {
                                _this.camera.rotation.x = 0;
                            } });
                    }
                    for (var j = 0; j < this.collisionActors.length; j++) {
                        var actorMesh = this.collisionActors[j].mesh;
                        if (actorMesh != this.mesh && actorMesh.intersectsPoint(b.position)) {
                            this.removeBall(b);
                        }
                    }
                    if (!this._isAlive && this.balls.length === 0) {
                        vt.GameController.getInstance().removeActor(this);
                    }
                }
                if (this._isAlive && this.scan() && this.canThrow) {
                    this.attack();
                }
            };
            Snowman.prototype.attack = function () {
                var _this = this;
                this.canThrow = false;
                this.mesh.skeleton.beginAnimation("ArmatureAction.001", false, 1, function () {
                    vt.SoundManager.getInstance().snowmanThrow(_this.mesh);
                    _this.timeOut = window.setTimeout(function () {
                        _this.canThrow = true;
                    }, Math.floor(Math.random() * 500) + 500);
                });
                window.setTimeout(function () {
                    for (var i = 0; i < 7; i++) {
                        var b = _this.makeSnowball();
                        if (i > 0) {
                            b.rotation.y += (Math.random() * 5 - Math.random() * 5) * Math.PI / 180;
                            b.rotation.x += (Math.random() * 3) * Math.PI / 180;
                        }
                    }
                }, 800);
            };
            Snowman.prototype.removeBall = function (b) {
                for (var i = this.balls.length - 1; i >= 0; i--) {
                    if (this.balls[i] === b) {
                        this.balls.splice(i, 1);
                        b.dispose();
                        return;
                    }
                }
            };
            Snowman.prototype.makeSnowball = function () {
                var b = this.scene.getMeshByName("item-snowball").createInstance("b");
                b.scaling = new BABYLON.Vector3(1, 1, 1);
                this.setToBonePos(b.position, this.bone, this.mesh.getWorldMatrix());
                b.position.y += .5;
                b.lookAt(this.camera.position);
                var distPos = this.camera.position.subtract(b.position).normalize();
                distPos = distPos.divide(new BABYLON.Vector3(3, 3, 3));
                b.vect = distPos;
                b.gravity = 0.0;
                b.gravityIncrease = 0.0;
                b.speed = .6 + Math.random() / 5;
                b.time = 0;
                this.balls.push(b);
                return b;
            };
            Snowman.prototype.setToBonePos = function (position, bone, meshMat) {
                var wmat = BABYLON.Matrix.Identity();
                var parentBone = bone.getParent();
                wmat.copyFrom(bone.getLocalMatrix());
                if (parentBone) {
                    wmat.multiplyToRef(parentBone.getAbsoluteTransform(), wmat);
                }
                wmat.multiplyToRef(meshMat, wmat);
                position.x = wmat.m[12];
                position.y = wmat.m[13];
                position.z = wmat.m[14];
                Snowman.updateBoneMatrix(bone);
            };
            Snowman.updateBoneMatrix = function (bone) {
                if (bone.getParent()) {
                    bone.getLocalMatrix().multiplyToRef(bone.getParent().getAbsoluteTransform(), bone.getAbsoluteTransform());
                }
                var children = bone.children;
                var len = children.length;
                for (var i = 0; i < len; i++) {
                    Snowman.updateBoneMatrix(children[i]);
                }
            };
            return Snowman;
        }(actors.Actor));
        actors.Snowman = Snowman;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var Container = (function (_super) {
            __extends(Container, _super);
            function Container(mesh, scene) {
                _super.call(this, mesh, scene);
            }
            Container.prototype.update = function () {
            };
            return Container;
        }(actors.Actor));
        actors.Container = Container;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var ChristmasTree = (function (_super) {
            __extends(ChristmasTree, _super);
            function ChristmasTree(mesh, scene) {
                _super.call(this, mesh, scene);
                this.count = 0;
                this.makeParticles();
            }
            ChristmasTree.prototype.update = function () {
                this.count++;
                if (this.count > 50) {
                    this.count = 0;
                    this.twinkle();
                }
            };
            ChristmasTree.prototype.makeParticles = function () {
                var _this = this;
                var tree = this.mesh;
                var verts = tree._geometry._meshes[0].getVerticesData(BABYLON.VertexBuffer.PositionKind);
                var len = verts.length;
                var vecsObj = {};
                var vecs = [];
                var mult = new BABYLON.Vector3(.1, .1, .1);
                for (var i = 0; i < len; i += 3) {
                    var v = new BABYLON.Vector3(verts[i], verts[i + 1], verts[i + 2]).add(tree.position).clone();
                    v.y += .2;
                    vecsObj[v.toString()] = v.clone();
                }
                for (var p in vecsObj) {
                    vecs.push(vecsObj[p]);
                }
                this.ps = new BABYLON.ParticleSystem("particles", vecs.length, this.scene);
                this.ps.particleTexture = new BABYLON.Texture("assets/star.jpg", this.scene);
                this.ps.manualEmitCount = vecs.length;
                this.ps.color1 = new BABYLON.Color4(0.7, 0.8, 1.0, 1.0);
                this.ps.color2 = new BABYLON.Color4(0.2, 0.5, 1.0, 1.0);
                this.ps.emitter = tree;
                this.ps.minEmitBox = new BABYLON.Vector3(-2, 0, -2);
                this.ps.maxEmitBox = new BABYLON.Vector3(2, 4, 2);
                this.ps.minSize = .7;
                this.ps.maxSize = .7;
                this.ps.minLifeTime = 1000;
                this.ps.maxLifeTime = 1000;
                this.ps.minAngularSpeed = 0;
                this.ps.maxAngularSpeed = 0;
                this.ps.minEmitPower = 0;
                this.ps.maxEmitPower = 0;
                this.ps.updateSpeed = 0.005;
                this.ps.start();
                setTimeout(function () {
                    for (var i = 0; i < vecs.length; i++) {
                        var p = _this.ps.particles[i];
                        p.position = vecs[i];
                    }
                }, 2500);
            };
            ChristmasTree.prototype.twinkle = function () {
                var len = this.ps.particles.length;
                for (var i = 0; i < len; i++) {
                    this.ps.particles[i].color = new BABYLON.Color4(Math.random() * 2, Math.random() * 2, Math.random() * 2, 1);
                }
            };
            ChristmasTree.getRand = function () {
                return (Math.random() - Math.random()) / 3;
            };
            return ChristmasTree;
        }(actors.Actor));
        actors.ChristmasTree = ChristmasTree;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var Crane = (function (_super) {
            __extends(Crane, _super);
            function Crane(mesh, scene, settings) {
                _super.call(this, mesh, scene, settings);
                this.initPos = 0;
                this.endPos = 0;
                this.moveVect0 = new BABYLON.Vector3(0, 0, -4);
            }
            Crane.prototype.initActor = function () {
                this.mesh.position.z -= 6;
                this.initPos = this.mesh.position.z;
                this.endPos = -this.settings.size.height / 5 + 16 + this.initPos;
                T._(this.mesh.position, 20 + Math.floor(Math.random() * 5), [{ prop: "z", from: this.initPos, to: this.endPos }], { yoyo: true, repeat: -1, delay: 1 + Math.floor(Math.random() * 3) });
            };
            Crane.prototype.update = function () {
                if (this.mesh.intersectsMesh(this.camera.cbox)) {
                }
            };
            return Crane;
        }(actors.Actor));
        actors.Crane = Crane;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var Truck = (function (_super) {
            __extends(Truck, _super);
            function Truck(mesh, scene) {
                _super.call(this, mesh, scene);
            }
            Truck.prototype.update = function () {
            };
            return Truck;
        }(actors.Actor));
        actors.Truck = Truck;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var actors;
    (function (actors) {
        var MeshFactory = (function () {
            function MeshFactory() {
            }
            MeshFactory.getActor = function (mesh, type, scene, settings) {
                if (settings === void 0) { settings = null; }
                switch (type) {
                    case MeshFactory.MESH_CONTAINER:
                        return new actors.Container(mesh, scene);
                    case MeshFactory.MESH_CRANE:
                        return new actors.Crane(mesh, scene, settings);
                    case MeshFactory.MESH_SNOWMAN:
                        return new actors.Snowman(mesh, scene);
                    case MeshFactory.MESH_TRUCK:
                        return new actors.Truck(mesh, scene);
                    case MeshFactory.MESH_SNOWBALL:
                        return new actors.Snowball(mesh, scene);
                    case MeshFactory.MESH_TREE:
                        return new actors.ChristmasTree(mesh, scene);
                }
                return null;
            };
            MeshFactory.MESH_CRANE = "crane";
            MeshFactory.MESH_CONTAINER = "container";
            MeshFactory.MESH_SNOWMAN = "snowman";
            MeshFactory.MESH_SNOWBALL = "snowball";
            MeshFactory.MESH_TRUCK = "truck";
            MeshFactory.MESH_TREE = "exit";
            return MeshFactory;
        }());
        actors.MeshFactory = MeshFactory;
    })(actors = vt.actors || (vt.actors = {}));
})(vt || (vt = {}));
var vt;
(function (vt) {
    var Event = vt.events.VTEvent;
    var MeshFactory = vt.actors.MeshFactory;
    var VTEvent = vt.events.VTEvent;
    var Snowman = vt.actors.Snowman;
    var GameController = (function (_super) {
        __extends(GameController, _super);
        function GameController() {
            _super.call(this);
            this.clonableMeshes = {};
            this.snowEmiterSize = 50;
            this.snowEmiterY = 10;
            this._isTouchDevice = false;
            this._isDebugMode = false;
            this._isDebugChecked = false;
            this.balls = [];
            this.pressTime = 0;
            this._setupDone = false;
            this.isWalkSoundPlaying = false;
            this.pointerDown = "mousedown";
            this.pointerUp = "mouseup";
            this.pointerMove = "mousemove";
            this.rotPosX0 = 0;
            this.rotPosY0 = 0;
            this.radY = 0;
            this.radX = 0;
            this.movePosX0 = 0;
            this.movePosY0 = 0;
            this.moveX = 0;
            this.moveZ = 0;
            GameController._instance = this;
            this.canvas = document.getElementById("canvas");
            this.soundManager = vt.SoundManager.getInstance();
            this.resizeHandler = this.resizeHandler.bind(this);
            window.addEventListener("resize", this.resizeHandler);
            this.setControls();
            GameController.setRay();
        }
        GameController.getInstance = function () {
            return GameController._instance;
        };
        GameController.prototype.run = function () {
            var x = this.camera.position.x;
            var y = this.camera.position.y;
            var z = this.camera.position.z;
            this.camera.cbox.position.x = x;
            this.camera.cbox.position.y = y;
            this.camera.cbox.position.z = z;
            this.camera.cbox.computeWorldMatrix();
            this.snowEmiter.position.x = x;
            this.snowEmiter.position.z = z;
            for (var i = 0; i < this.actors.length; i++) {
                this.actors[i].update();
            }
            this.camera.rotation.y += this.radY;
            this.camera.rotation.x += this.radX;
            if (this.camera._localDirection) {
                this.camera._localDirection.copyFromFloats(this.moveX, 0, -this.moveZ);
                this.camera.getViewMatrix().invertToRef(this.camera._cameraTransformMatrix);
                BABYLON.Vector3.TransformNormalToRef(this.camera._localDirection, this.camera._cameraTransformMatrix, this.camera._transformedDirection);
                this.camera.cameraDirection.addInPlace(this.camera._transformedDirection);
            }
            var rx = this.camera.rotation.x;
            if (rx <= -.35) {
                this.camera.rotation.x = -.35;
            }
            else if (rx >= .25) {
                this.camera.rotation.x = .25;
            }
            this.checkPickups();
            this.checkExit();
            this.checkBalls();
            this.pressTime++;
            var speed = (Math.abs(this.camera._oldPosition.z - this.camera._newPosition.z) + Math.abs(this.camera._oldPosition.x - this.camera._newPosition.x));
            if (!this.isWalkSoundPlaying && speed > 0.1) {
                this.soundManager.playerWalk();
                this.isWalkSoundPlaying = true;
            }
            else if (this.isWalkSoundPlaying && speed < 0.1) {
                this.soundManager.stopPlayerWalk();
                this.isWalkSoundPlaying = false;
            }
        };
        GameController.prototype.destroy = function () {
            if (this.scene) {
                this.scene.onPointerUp = this.scene.onPointerDown = null;
                this.scene.dispose();
            }
        };
        GameController.prototype.initLevel = function (scene, engine, data, startEngine) {
            if (startEngine === void 0) { startEngine = true; }
            var camera;
            if (this._isTouchDevice) {
                camera = new BABYLON.FreeCamera("Cam", new BABYLON.Vector3(0, .5, -20), scene);
                camera.inertia = 0.5;
                this.pointerDown = "touchstart";
                this.pointerUp = "touchend";
                this.pointerMove = "touchmove";
            }
            else {
                camera = new BABYLON.UniversalCamera("Cam", new BABYLON.Vector3(0, .5, -20), scene);
                camera.attachControl(this.canvas, false, false);
            }
            camera.fov = .6;
            camera.checkCollisions = true;
            camera.applyGravity = true;
            camera.ellipsoid = new BABYLON.Vector3(1, .5, 1);
            camera.cbox = BABYLON.Mesh.CreateBox("cbox", 1, scene);
            camera.cbox.scaling = new BABYLON.Vector3(.5, 2.1, .5);
            camera.cbox.isPickable = true;
            camera.cbox.isVisible = false;
            camera.position.y = 2;
            camera.keysUp = [87, 38];
            camera.keysDown = [83, 40];
            camera.keysLeft = [65, 37];
            camera.keysRight = [68, 39];
            camera.speed = 1.5;
            this.camera = camera;
            scene.activeCamera = this.camera;
            scene.clearColor = new BABYLON.Color3(.4, .45, .5);
            scene.ambientColor = new BABYLON.Color3(.2, .2, .2);
            var ground = scene.getMeshByName("ground");
            scene.gravity = new BABYLON.Vector3(0, -0.9, 0);
            scene.collisionsEnabled = true;
            ground.checkCollisions = true;
            this.snowEmiter = BABYLON.Mesh.CreateGround("snow-emmiter", this.snowEmiterSize, this.snowEmiterSize, 1, scene);
            this.snowEmiter.position.y = this.snowEmiterY;
            this.scene = scene;
            this.engine = engine;
            this.listClonableMeshes();
            this.parseLevel(data);
            if (startEngine) {
                this.startEngine();
            }
            if (!this._setupDone) {
            }
            this.setThrowBalls();
            this._isDebugMode = window.location.hash === "#d";
            if (this._isDebugMode && !this._isDebugChecked) {
                scene.debugLayer.show();
                this._isDebugChecked = true;
            }
            scene.workerCollisions = true;
            this._setupDone = true;
            document.getElementById("canvas").focus();
        };
        GameController.prototype.getCollisionActors = function () {
            return this.collisionActors;
        };
        GameController.prototype.pause = function () {
            this.engine.stopRenderLoop();
        };
        GameController.prototype.playerAddPoints = function (value) {
            this.dispatch(new VTEvent(VTEvent.POINTS_CHANGE, value));
        };
        Object.defineProperty(GameController.prototype, "isTouchDevice", {
            get: function () {
                return this._isTouchDevice;
            },
            set: function (value) {
                this._isTouchDevice = value;
            },
            enumerable: true,
            configurable: true
        });
        GameController.prototype.startEngine = function () {
            var _this = this;
            this.scene.registerBeforeRender(function () { return _this.run(); });
            this.engine.runRenderLoop(function () {
                _this.scene.render();
            });
        };
        GameController.prototype.parseLevel = function (json) {
            var _this = this;
            this.actors = [];
            this.collisionActors = [];
            this.collectables = [];
            this.hl = new BABYLON.HighlightLayer("hl1", this.scene);
            this.hl.blurHorizontalSize = 1;
            this.hl.blurVerticalSize = 1;
            for (var i = 0; i < json.items.length; i++) {
                var obj = json.items[i];
                var clone;
                switch (obj.type) {
                    case "camera":
                        var cam = this.scene.activeCamera;
                        cam.position.x = obj.pos.x;
                        cam.position.z = obj.pos.z;
                        break;
                    case "container":
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        clone = GameController.getClone(mesh, obj, i + "", true, "instance");
                        clone.isPickable = true;
                        var actor = MeshFactory.getActor(clone, obj.type, this.scene);
                        this.actors.push(actor);
                        this.collisionActors.push(actor);
                        var rand = Math.floor(Math.random() * 2);
                        if (rand === 1) {
                            clone.rotation.y += Math.PI;
                        }
                        if (obj.kind === "stack") {
                            for (var j = 1; j < 3; j++) {
                                var mesh2 = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                                var clone2 = GameController.getClone(mesh2, obj, j + "s", false);
                                clone2.position.y += j * 4;
                                var actor2 = MeshFactory.getActor(clone2, obj.type, this.scene);
                                this.actors.push(actor2);
                                this.collisionActors.push(actor2);
                            }
                        }
                        break;
                    case "snowman":
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        mesh.computeBonesUsingShaders = false;
                        var clone = GameController.getClone(mesh, obj, i + "", false, "clone");
                        clone.pos = clone.position.clone();
                        clone.skeleton = mesh.skeleton.clone("skeleton" + i);
                        var actor = MeshFactory.getActor(clone, obj.type, this.scene);
                        this.actors.push(actor);
                        clone.computeWorldMatrix();
                        break;
                    case "crane":
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone = GameController.getClone(mesh, obj, i + "", true, "instance");
                        var actor = MeshFactory.getActor(clone, obj.type, this.scene, obj);
                        actor.initActor();
                        this.actors.push(actor);
                        setTimeout(function () {
                            _this.soundManager.playCrane(actor.mesh);
                        }, 700);
                        break;
                    case "crane2":
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone = GameController.getClone(mesh, obj, i + "", true, "clone");
                        break;
                    case "collectable":
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone = GameController.getClone(mesh, obj, i + "", false, "instance");
                        clone.rotation.y = Math.random();
                        clone.presentValue = 15;
                        this.collectables.push(clone);
                        break;
                    case "collectableBigger":
                        var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone = GameController.getClone(mesh, obj, i + "", false, "clone");
                        clone.rotation.y = Math.random();
                        clone.presentValue = 100;
                        this.hl.addMesh(clone, BABYLON.Color3.White());
                        this.collectables.push(clone);
                        break;
                    case "exit":
                        var exit = this.scene.getMeshByName("exit");
                        exit.position.x = obj.pos.x;
                        exit.position.z = obj.pos.z;
                        this.exit = exit;
                        var actor = MeshFactory.getActor(exit, obj.type, this.scene);
                        this.actors.push(actor);
                        setTimeout(function () {
                            _this.soundManager.playCloseToExit(exit.position);
                        }, 700);
                        break;
                    case "announcer":
                        this.soundManager.makeAnouncer(obj.pos, false);
                        mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                        var clone = GameController.getClone(mesh, obj, i + "", false, "instance");
                        break;
                    case "truck":
                        if (!this.isTouchDevice) {
                            var mesh = this.clonableMeshes[obj.type][GameController.getRand(this.clonableMeshes[obj.type].length)];
                            var clone = GameController.getClone(mesh, obj, i + "", true, "instance");
                            var actor = MeshFactory.getActor(clone, obj.type, this.scene);
                            this.actors.push(actor);
                            this.collisionActors.push(actor);
                        }
                        break;
                }
            }
            this.makeFog(json.fog);
            this.makeSnow();
            this.dispatch(new Event(Event.SCENE_READY, { scene: this.scene, engine: this.engine }, this));
            this.scene.getMeshByName("item-snowman").dispose();
            if (this.isTouchDevice) {
                this.scene.getMeshByName("item-truck").dispose();
            }
            else {
                this.scene.getMeshByName("item-truck").setEnabled(false);
            }
            this.hl.addMesh(this.scene.getMeshByName("star"), BABYLON.Color3.Yellow());
            console.log("collision meshes:" + this.collisionActors.length);
        };
        GameController.getClone = function (mesh, obj, name, checkCollisions, type) {
            if (checkCollisions === void 0) { checkCollisions = true; }
            if (type === void 0) { type = "instance"; }
            var clone;
            if (type === "instance") {
                clone = mesh.createInstance(mesh.name + "_" + name);
            }
            else {
                clone = mesh.clone(mesh.name + "_" + name);
            }
            clone.position.x = obj.pos.x;
            clone.position.z = obj.pos.z;
            clone.rotation.y = obj.rotation.y;
            clone.checkCollisions = checkCollisions;
            clone.scaling.x = 1;
            clone.scaling.y = 1;
            clone.scaling.z = 1;
            return clone;
        };
        GameController.prototype.listClonableMeshes = function () {
            var list = this.scene.meshes;
            this.clonableMeshes = {};
            for (var i = 0; i < list.length; i++) {
                var mesh = list[i];
                if (mesh.name.indexOf("item-") > -1) {
                    var name = mesh.name.split("-")[1];
                    if (!this.clonableMeshes.hasOwnProperty(name)) {
                        this.clonableMeshes[name] = [];
                    }
                    this.clonableMeshes[name].push(mesh);
                    mesh.setEnabled(false);
                }
            }
        };
        GameController.prototype.makeSnow = function () {
            var particleSystem = new BABYLON.ParticleSystem("particles", 2000, this.scene);
            particleSystem.particleTexture = new BABYLON.Texture("assets/snowflake.png", this.scene);
            particleSystem.emitter = this.snowEmiter;
            particleSystem.emitRate = 500;
            particleSystem.minEmitBox = new BABYLON.Vector3(-this.snowEmiterSize, 0, -this.snowEmiterSize);
            particleSystem.maxEmitBox = new BABYLON.Vector3(this.snowEmiterSize, 0, this.snowEmiterSize);
            particleSystem.gravity = new BABYLON.Vector3(0, -5, 0);
            particleSystem.minSize = 0.08;
            particleSystem.maxSize = 0.2;
            particleSystem.minLifeTime = 2;
            particleSystem.maxLifeTime = 5;
            particleSystem.direction1 = new BABYLON.Vector3(0, -8, 0);
            particleSystem.direction2 = new BABYLON.Vector3(0, -8, 0);
            particleSystem.start();
        };
        GameController.prototype.makeFog = function (fogFar) {
            if (fogFar === void 0) { fogFar = 90.0; }
            this.scene.fogMode = BABYLON.Scene.FOGMODE_LINEAR;
            this.scene.fogStart = 10.0;
            this.scene.fogEnd = fogFar;
            this.scene.fogColor = new BABYLON.Color3(0.4, 0.45, 0.5);
        };
        GameController.prototype.checkPickups = function () {
            for (var i = 0; i < this.collectables.length; i++) {
                var item = this.collectables[i];
                if (item.getDistanceToCamera(this.camera) < 2) {
                    this.dispatch(new VTEvent(VTEvent.POINTS_CHANGE, item.presentValue));
                    this.collectables.splice(i, 1);
                    this.soundManager.pickPresent();
                    item.dispose();
                }
            }
        };
        GameController.prototype.checkExit = function () {
            if (this.exit && this.exit.getDistanceToCamera(this.camera) < 5) {
                this.dispatch(new VTEvent(VTEvent.LEVEL_COMPLETE));
            }
        };
        GameController.getRand = function (num) {
            return Math.floor(Math.random() * num);
        };
        GameController.prototype.resizeHandler = function () {
            if (this.engine)
                this.engine.resize();
            this.updateScreenSize();
        };
        GameController.prototype.updateScreenSize = function () {
            this.w = window.innerWidth / 2;
            this.h = window.innerHeight / 2;
        };
        GameController.prototype.setThrowBalls = function () {
            var _this = this;
            var wall = BABYLON.Mesh.CreatePlane("shootTarget", 200.0, this.scene);
            wall.material = new BABYLON.StandardMaterial("$texture1", this.scene);
            wall.material.alpha = 0;
            wall.position.z = 10;
            wall.parent = this.camera;
            this.hl.addExcludedMesh(wall);
            this.scene.onPointerObservable.add(function () {
                _this.pressTime = 0;
            }, BABYLON.PointerEventTypes.POINTERDOWN);
            this.scene.onPointerObservable.add(function (evt) {
                if (_this.pressTime < 12 && evt.pickInfo.hit) {
                    var b = _this.scene.getMeshByName("item-snowball").createInstance("sss");
                    b.position.copyFrom(_this.camera.position);
                    b.lookAt(evt.pickInfo.pickedPoint);
                    b.gravity = .05;
                    b.speed = 1;
                    b.time = 150;
                    _this.balls.push(b);
                    _this.soundManager.throwBall();
                }
            }, BABYLON.PointerEventTypes.POINTERUP);
        };
        GameController.prototype.checkBalls = function () {
            for (var i = 0; i < this.balls.length; i++) {
                var b = this.balls[i];
                b.translate(BABYLON.Axis.Z, -b.speed, BABYLON.Space.LOCAL);
                b.translate(BABYLON.Axis.Y, b.gravity, BABYLON.Space.LOCAL);
                b.gravity -= .004;
                b.speed -= .008;
                b.time--;
                if (b.time < 0) {
                    this.removeBall(b);
                }
                for (var j = 0; j < this.actors.length; j++) {
                    if (b.intersectsMesh(this.actors[j].mesh)) {
                        if (this.actors[j] instanceof Snowman) {
                            this.actors[j].hit();
                            this.soundManager.snowmanHit(this.actors[j].mesh);
                        }
                        this.removeBall(b);
                    }
                }
            }
        };
        GameController.prototype.removeBall = function (b) {
            this.balls.splice(this.balls.indexOf(b), 1);
            b.dispose();
        };
        GameController.prototype.removeActor = function (actor) {
            var ind = this.actors.indexOf(actor);
            if (ind > -1) {
                this.actors.splice(ind, 1);
            }
        };
        GameController.prototype.setControls = function () {
            this.rotStart = this.rotStart.bind(this);
            this.rotRun = this.rotRun.bind(this);
            this.rotEnd = this.rotEnd.bind(this);
            this.moveStart = this.moveStart.bind(this);
            this.moveRun = this.moveRun.bind(this);
            this.moveEnd = this.moveEnd.bind(this);
            this.divMove = document.getElementById("c-move");
            this.divRotate = document.getElementById("c-rotate");
            this.divRotate.addEventListener("touchstart", this.rotStart);
            this.divRotate.addEventListener("touchend", this.rotEnd);
            this.divMove.addEventListener("touchstart", this.moveStart);
            this.divMove.addEventListener("touchend", this.moveEnd);
        };
        GameController.prototype.rotStart = function (e) {
            this.rotPosX0 = e.touches[0].clientX;
            this.rotPosY0 = e.touches[0].clientY;
            this.divRotate.addEventListener("touchmove", this.rotRun);
            e.preventDefault();
        };
        GameController.prototype.rotRun = function (e) {
            e.preventDefault();
            if (!this.camera)
                return;
            var touch = e.touches[0];
            var speedY = (touch.clientX - this.rotPosX0) / 700;
            var speedYAbs = Math.abs(speedY);
            if (speedYAbs > .04 && speedY > 0) {
                speedY = .04;
            }
            else if (speedYAbs > .04 && speedY < 0) {
                speedY = -.04;
            }
            this.radY = speedY;
            var speedX = (touch.clientY - this.rotPosY0) / 2000;
            var speedXAbs = Math.abs(speedX);
            if (speedYAbs < 0.01) {
                if (speedXAbs > .03 && speedX > 0) {
                    speedX = .03;
                }
                else if (speedXAbs > .04 && speedX < 0) {
                    speedX = -.04;
                }
            }
        };
        GameController.prototype.rotEnd = function (e) {
            this.divRotate.removeEventListener("touchmove", this.rotRun);
            this.radX = 0;
            this.radY = 0;
        };
        GameController.prototype.moveStart = function (e) {
            this.movePosX0 = e.touches[0].clientX;
            this.movePosY0 = e.touches[0].clientY;
            this.divMove.addEventListener("touchmove", this.moveRun);
            e.preventDefault();
        };
        GameController.prototype.moveRun = function (e) {
            e.preventDefault();
            if (!this.camera)
                return;
            var touch = e.touches[0];
            var speedZ = (touch.clientY - this.movePosY0) / 70;
            var speedAbsZ = Math.abs(speedZ);
            this.moveZ = speedZ;
            var speedX = (touch.clientX - this.movePosX0) / 100;
            var speedXAbs = Math.abs(speedX);
            if (speedAbsZ < 0.2) {
                this.moveX = speedX;
            }
        };
        GameController.prototype.moveEnd = function (e) {
            this.divMove.removeEventListener("touchmove", this.moveRun);
            this.moveX = 0;
            this.moveZ = 0;
        };
        GameController.setRay = function () {
            var R = BABYLON.Ray;
            R.prototype.rayLine = null;
            R.prototype.intersectsMesh = function (mesh, fastCheck) {
                var ray = BABYLON.Ray.Transform(this, mesh.getWorldMatrix().clone().invert());
                return mesh.intersects(ray, fastCheck);
            };
            R.prototype.intersectsMeshes = function (meshes, fastCheck) {
                var arr = [];
                for (var i = 0; i < meshes.length; i++) {
                    var ray = BABYLON.Ray.Transform(this, meshes[i].getWorldMatrix().clone().invert());
                    var pick = meshes[i].intersects(ray, fastCheck);
                    if (pick.hit) {
                        arr.push(pick);
                    }
                }
                arr.sort(R.pickSortFunc);
                return arr;
            };
            R.pickSortFunc = function (a, b) {
                if (a.distance > b.distance) {
                    return 1;
                }
                else if (a.distance < b.distance) {
                    return -1;
                }
                return 0;
            };
            R.prototype.show = function (scene, color) {
                var ps = [this.origin, this.origin.add(this.direction.scale(this.length))];
                this.rayLine = BABYLON.Mesh.CreateLines("rayLine", ps, scene, true);
                this.rayLine.color = color || new BABYLON.Color3(1, 0, 0);
            };
            R.prototype.update = function () {
                if (this.rayLine) {
                    this.rayLine = BABYLON.Mesh.CreateLines(null, [this.origin, this.origin.add(this.direction.scale(this.length))], null, null, this.rayLine);
                }
            };
            R.prototype.setDirectionFromMesh = function (mesh) {
                this.direction = mesh.getDirection(new BABYLON.Vector3(0, 0, -1));
            };
        };
        return GameController;
    }(vt.events.Dispatcher));
    vt.GameController = GameController;
})(vt || (vt = {}));
var vt;
(function (vt) {
    var VTEvent = vt.events.VTEvent;
    var Service = (function (_super) {
        __extends(Service, _super);
        function Service(enforcer) {
            _super.call(this);
            this.localStorageAvailable = Service.checkStorageAvailable();
            this.sendQuery("func=checkStatus");
        }
        Service.getInstance = function () {
            if (!Service.__instance) {
                Service.__instance = new Service(new Enforcer2());
            }
            return Service.__instance;
        };
        Service.prototype.getScores = function () {
            this.sendQuery("func=getResults");
        };
        Service.prototype.sendScore = function (query) {
            this.sendQuery("func=sendScore&query=" + query);
        };
        Service.prototype.sendQuery = function (query) {
            var _this = this;
            var xhr = new XMLHttpRequest();
            xhr.addEventListener("load", function (evt) { return _this.xhrResponseHandler(evt); });
            xhr.addEventListener("error", function (evt) { return _this.xhrResponseHandler(evt); });
            xhr.open("POST", "api/");
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhr.send(query);
        };
        Service.prototype.xhrResponseHandler = function (evt) {
            if (evt.target.responseText.indexOf("Cannot POST") > -1) {
                this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, { type: VTEvent.SERVER_NO_CONN }));
                return;
            }
            var res;
            try {
                res = JSON.parse(evt.target.responseText);
            }
            catch (er) {
                console.log(er);
                return;
            }
            switch (res.type) {
                case "competitionExpired":
                    if (res.response === true) {
                        this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, { type: VTEvent.SERVER_COMPETITION_EXPIRED }));
                    }
                    break;
                case "dbInsert":
                    this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, { type: VTEvent.SERVER_SCORE_SENT }));
                    break;
                case "dbResult":
                    this.dispatch(new VTEvent(VTEvent.SERVER_RESULT, { type: VTEvent.SERVER_COMPETITION_RESULT, result: res.response }));
                    break;
                case "error":
                    console.log("Error:", res.response);
                    break;
            }
        };
        Service.prototype.xhrErrorHandler = function (evt) {
            console.log(evt.target);
        };
        Service.prototype.setLocalData = function (data) {
            if (this.localStorageAvailable) {
                window.localStorage.setItem(Service.LOCAL_ID, JSON.stringify(data));
            }
        };
        Service.prototype.getLocalData = function () {
            if (this.localStorageAvailable) {
                return JSON.parse(window.localStorage.getItem(Service.LOCAL_ID));
            }
            return null;
        };
        Service.checkStorageAvailable = function () {
            try {
                var storage = window.localStorage, x = '__storage_test__';
                storage.setItem(x, x);
                storage.removeItem(x);
                return true;
            }
            catch (e) {
                console.log("localStorage is unavailable.");
                return false;
            }
        };
        Service.LOCAL_ID = "__VT_GAME_2016__";
        return Service;
    }(vt.events.Dispatcher));
    vt.Service = Service;
})(vt || (vt = {}));
var Enforcer2 = (function () {
    function Enforcer2() {
    }
    return Enforcer2;
}());
var vt;
(function (vt) {
    var Lists = (function () {
        function Lists() {
        }
        Lists.check = function (str) {
            var exp = /^[a-zA-Z0-9 -]+$/gi;
            if (!str.match(exp)) {
                return false;
            }
            var name = str.toLowerCase().split(/[ -]/g);
            for (var i = 0; i < name.length; i++) {
                if (Lists.list.hasOwnProperty(name[i])) {
                    return false;
                }
            }
            return true;
        };
        Lists.list = {
            fuckyou: 1,
            "4r5e": 1,
            "5h1t": 1,
            "5hit": 1,
            a55: 1,
            anal: 1,
            anus: 1,
            ar5e: 1,
            arrse: 1,
            arse: 1,
            ass: 1,
            "ass-fucker": 1,
            asses: 1,
            assfucker: 1,
            assfukka: 1,
            asshole: 1,
            assholes: 1,
            asswhole: 1,
            a_s_s: 1,
            "b!tch": 1,
            b00bs: 1,
            b17ch: 1,
            b1tch: 1,
            ballbag: 1,
            balls: 1,
            ballsack: 1,
            bastard: 1,
            beastial: 1,
            beastiality: 1,
            bellend: 1,
            bestial: 1,
            bestiality: 1,
            "bi+ch": 1,
            biatch: 1,
            bitch: 1,
            bitcher: 1,
            bitchers: 1,
            bitches: 1,
            bitchin: 1,
            bitching: 1,
            bloody: 1,
            "blow job": 1,
            blowjob: 1,
            blowjobs: 1,
            boiolas: 1,
            bollock: 1,
            bollok: 1,
            boner: 1,
            boob: 1,
            boobs: 1,
            booobs: 1,
            boooobs: 1,
            booooobs: 1,
            booooooobs: 1,
            breasts: 1,
            buceta: 1,
            bugger: 1,
            bum: 1,
            "bunny fucker": 1,
            butt: 1,
            butthole: 1,
            buttmuch: 1,
            buttplug: 1,
            c0ck: 1,
            c0cksucker: 1,
            "carpet muncher": 1,
            cawk: 1,
            chink: 1,
            cipa: 1,
            cl1t: 1,
            clit: 1,
            clitoris: 1,
            clits: 1,
            cnut: 1,
            cock: 1,
            "cock-sucker": 1,
            cockface: 1,
            cockhead: 1,
            cockmunch: 1,
            cockmuncher: 1,
            cocks: 1,
            "cocksuck ": 1,
            "cocksucked ": 1,
            cocksucker: 1,
            cocksucking: 1,
            "cocksucks ": 1,
            cocksuka: 1,
            cocksukka: 1,
            cok: 1,
            cokmuncher: 1,
            coksucka: 1,
            coon: 1,
            cox: 1,
            crap: 1,
            cum: 1,
            cummer: 1,
            cumming: 1,
            cums: 1,
            cumshot: 1,
            cunilingus: 1,
            cunillingus: 1,
            cunnilingus: 1,
            cunt: 1,
            "cuntlick ": 1,
            "cuntlicker ": 1,
            "cuntlicking ": 1,
            cunts: 1,
            cyalis: 1,
            cyberfuc: 1,
            "cyberfuck ": 1,
            "cyberfucked ": 1,
            cyberfucker: 1,
            cyberfuckers: 1,
            "cyberfucking ": 1,
            d1ck: 1,
            damn: 1,
            dick: 1,
            dickhead: 1,
            dildo: 1,
            dildos: 1,
            dink: 1,
            dinks: 1,
            dirsa: 1,
            dlck: 1,
            "dog-fucker": 1,
            doggin: 1,
            dogging: 1,
            donkeyribber: 1,
            doosh: 1,
            duche: 1,
            dyke: 1,
            ejaculate: 1,
            ejaculated: 1,
            "ejaculates ": 1,
            "ejaculating ": 1,
            ejaculatings: 1,
            ejaculation: 1,
            ejakulate: 1,
            "f u c k": 1,
            "f u c k e r": 1,
            f4nny: 1,
            fag: 1,
            fagging: 1,
            faggitt: 1,
            faggot: 1,
            faggs: 1,
            fagot: 1,
            fagots: 1,
            fags: 1,
            fanny: 1,
            fannyflaps: 1,
            fannyfucker: 1,
            fanyy: 1,
            fatass: 1,
            fcuk: 1,
            fcuker: 1,
            fcuking: 1,
            feck: 1,
            fecker: 1,
            felching: 1,
            fellate: 1,
            fellatio: 1,
            "fingerfuck ": 1,
            "fingerfucked ": 1,
            "fingerfucker ": 1,
            fingerfuckers: 1,
            "fingerfucking ": 1,
            "fingerfucks ": 1,
            fistfuck: 1,
            "fistfucked ": 1,
            "fistfucker ": 1,
            "fistfuckers ": 1,
            "fistfucking ": 1,
            "fistfuckings ": 1,
            "fistfucks ": 1,
            flange: 1,
            fook: 1,
            fooker: 1,
            fuck: 1,
            fucka: 1,
            fucked: 1,
            fucker: 1,
            fuckers: 1,
            fuckhead: 1,
            fuckheads: 1,
            fuckin: 1,
            fucking: 1,
            fuckings: 1,
            fuckingshitmotherfucker: 1,
            "fuckme ": 1,
            fucks: 1,
            fuckwhit: 1,
            fuckwit: 1,
            "fudge packer": 1,
            fudgepacker: 1,
            fuk: 1,
            fuker: 1,
            fukker: 1,
            fukkin: 1,
            fuks: 1,
            fukwhit: 1,
            fukwit: 1,
            fux: 1,
            fux0r: 1,
            f_u_c_k: 1,
            gangbang: 1,
            "gangbanged ": 1,
            "gangbangs ": 1,
            gaylord: 1,
            gaysex: 1,
            goatse: 1,
            God: 1,
            "god-dam": 1,
            "god-damned": 1,
            goddamn: 1,
            goddamned: 1,
            "hardcoresex ": 1,
            hell: 1,
            heshe: 1,
            hoar: 1,
            hoare: 1,
            hoer: 1,
            homo: 1,
            hore: 1,
            horniest: 1,
            horny: 1,
            hotsex: 1,
            "jack-off ": 1,
            jackoff: 1,
            jap: 1,
            "jerk-off ": 1,
            jism: 1,
            "jiz ": 1,
            "jizm ": 1,
            jizz: 1,
            kawk: 1,
            knob: 1,
            knobead: 1,
            knobed: 1,
            knobend: 1,
            knobhead: 1,
            knobjocky: 1,
            knobjokey: 1,
            kock: 1,
            kondum: 1,
            kondums: 1,
            kum: 1,
            kummer: 1,
            kumming: 1,
            kums: 1,
            kunilingus: 1,
            "l3i+ch": 1,
            l3itch: 1,
            labia: 1,
            lmfao: 1,
            lust: 1,
            lusting: 1,
            m0f0: 1,
            m0fo: 1,
            m45terbate: 1,
            ma5terb8: 1,
            ma5terbate: 1,
            masochist: 1,
            "master-bate": 1,
            masterb8: 1,
            "masterbat*": 1,
            masterbat3: 1,
            masterbate: 1,
            masterbation: 1,
            masterbations: 1,
            masturbate: 1,
            "mo-fo": 1,
            mof0: 1,
            mofo: 1,
            mothafuck: 1,
            mothafucka: 1,
            mothafuckas: 1,
            mothafuckaz: 1,
            "mothafucked ": 1,
            mothafucker: 1,
            mothafuckers: 1,
            mothafuckin: 1,
            "mothafucking ": 1,
            mothafuckings: 1,
            mothafucks: 1,
            "mother fucker": 1,
            motherfuck: 1,
            motherfucked: 1,
            motherfucker: 1,
            motherfuckers: 1,
            motherfuckin: 1,
            motherfucking: 1,
            motherfuckings: 1,
            motherfuckka: 1,
            motherfucks: 1,
            muff: 1,
            mutha: 1,
            muthafecker: 1,
            muthafuckker: 1,
            muther: 1,
            mutherfucker: 1,
            n1gga: 1,
            n1gger: 1,
            nazi: 1,
            nigg3r: 1,
            nigg4h: 1,
            nigga: 1,
            niggah: 1,
            niggas: 1,
            niggaz: 1,
            nigger: 1,
            "niggers ": 1,
            nob: 1,
            "nob jokey": 1,
            nobhead: 1,
            nobjocky: 1,
            nobjokey: 1,
            numbnuts: 1,
            nutsack: 1,
            "orgasim ": 1,
            "orgasims ": 1,
            orgasm: 1,
            "orgasms ": 1,
            p0rn: 1,
            pawn: 1,
            pecker: 1,
            penis: 1,
            penisfucker: 1,
            phonesex: 1,
            phuck: 1,
            phuk: 1,
            phuked: 1,
            phuking: 1,
            phukked: 1,
            phukking: 1,
            phuks: 1,
            phuq: 1,
            pigfucker: 1,
            pimpis: 1,
            piss: 1,
            pissed: 1,
            pisser: 1,
            pissers: 1,
            "pisses ": 1,
            pissflaps: 1,
            "pissin ": 1,
            pissing: 1,
            "pissoff ": 1,
            poop: 1,
            porn: 1,
            porno: 1,
            pornography: 1,
            pornos: 1,
            prick: 1,
            "pricks ": 1,
            pron: 1,
            pube: 1,
            pusse: 1,
            pussi: 1,
            pussies: 1,
            pussy: 1,
            "pussys ": 1,
            rectum: 1,
            retard: 1,
            rimjaw: 1,
            rimming: 1,
            "s hit": 1,
            "s.o.b.": 1,
            sadist: 1,
            schlong: 1,
            screwing: 1,
            scroat: 1,
            scrote: 1,
            scrotum: 1,
            semen: 1,
            sex: 1,
            "sh!+": 1,
            "sh!t": 1,
            sh1t: 1,
            shag: 1,
            shagger: 1,
            shaggin: 1,
            shagging: 1,
            shemale: 1,
            "shi+": 1,
            shit: 1,
            shitdick: 1,
            shite: 1,
            shited: 1,
            shitey: 1,
            shitfuck: 1,
            shitfull: 1,
            shithead: 1,
            shiting: 1,
            shitings: 1,
            shits: 1,
            shitted: 1,
            shitter: 1,
            "shitters ": 1,
            shitting: 1,
            shittings: 1,
            "shitty ": 1,
            skank: 1,
            slut: 1,
            sluts: 1,
            smegma: 1,
            smut: 1,
            snatch: 1,
            "son-of-a-bitch": 1,
            spac: 1,
            spunk: 1,
            s_h_i_t: 1,
            t1tt1e5: 1,
            t1tties: 1,
            teets: 1,
            teez: 1,
            testical: 1,
            testicle: 1,
            tit: 1,
            titfuck: 1,
            tits: 1,
            titt: 1,
            tittie5: 1,
            tittiefucker: 1,
            titties: 1,
            tittyfuck: 1,
            tittywank: 1,
            titwank: 1,
            tosser: 1,
            turd: 1,
            tw4t: 1,
            twat: 1,
            twathead: 1,
            twatty: 1,
            twunt: 1,
            twunter: 1,
            v14gra: 1,
            v1gra: 1,
            vagina: 1,
            viagra: 1,
            vulva: 1,
            w00se: 1,
            wang: 1,
            wank: 1,
            wanker: 1,
            wanky: 1,
            whoar: 1,
            whore: 1,
            willies: 1,
            willy: 1,
            xrated: 1,
            xxx: 1
        };
        Lists.COUNTRY_LIST = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua &amp; Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia &amp; Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Cape Verde", "Cayman Islands", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cruise Ship", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kuwait", "Kyrgyz Republic", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Mauritania", "Mauritius", "Mexico", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Namibia", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre &amp; Miquelon", "Samoa", "San Marino", "Satellite", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "South Africa", "South Korea", "Spain", "Sri Lanka", "St Kitts &amp; Nevis", "St Lucia", "St Vincent", "St. Lucia", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad &amp; Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks &amp; Caicos", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Uruguay", "Uzbekistan", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
        return Lists;
    }());
    vt.Lists = Lists;
})(vt || (vt = {}));
var vt;
(function (vt) {
    var Event = vt.events.VTEvent;
    var VTEvent = vt.events.VTEvent;
    var Main = (function () {
        function Main() {
            this.currentLevel = 1;
            this.totalLevels = 3;
            this.points = 100;
            this.pointsCounter = 100;
            this.time = 0;
            this.time0 = 0;
            this.tTimer = 0;
            this.interval = 0;
            this.date = new Date();
            this.levels = [];
            this.score = 0;
            this.fieldsData = [];
            this.currentUser = "";
            this._isTouch = 0;
            this._competionEnded = false;
            this._canPoll = true;
            this._isPolling = false;
            this.init();
        }
        Main.prototype.init = function () {
            var _this = this;
            this.overlayDiv = document.getElementById("overlay");
            this.overlayDiv.addEventListener("click", function (e) { return _this.btnClickHandler(e); });
            this.timeHtml = document.getElementById("disp-time");
            this.scoreHtml = document.getElementById("disp-score");
            this.sceneReadyHanlder = this.sceneReadyHanlder.bind(this);
            this.levelCompleteHandler = this.levelCompleteHandler.bind(this);
            this.serverResponseHandler = this.serverResponseHandler.bind(this);
            this.pointsChangeHandler = this.pointsChangeHandler.bind(this);
            this.updateTime = this.updateTime.bind(this);
            this.assetManager = new vt.LevelLoader();
            this.gameController = new vt.GameController();
            this.soundManager = vt.SoundManager.getInstance();
            this.service = vt.Service.getInstance();
            this.assetManager.addListener(Event.SCENE_READY, this.sceneReadyHanlder);
            this.gameController.addListener(Event.LEVEL_COMPLETE, this.levelCompleteHandler);
            this.gameController.addListener(Event.POINTS_CHANGE, this.pointsChangeHandler);
            this.service.addListener(Event.SERVER_RESULT, this.serverResponseHandler);
            var preloader = document.getElementById("preloader");
            T._(preloader, .5, [{ prop: "scaleY", to: 0 }, { prop: "scaleZ", to: 1 }], { delay: 1 }, { call: function () {
                    preloader.parentNode.removeChild(preloader);
                } });
            this.timeHtml.innerHTML = "00:00";
            this.scoreHtml.innerHTML = this.points + "";
            this.touchTest = this.touchTest.bind(this);
            window.addEventListener('touchstart', this.touchTest);
            document.getElementById("twit-start").setAttribute("href", "https://twitter.com/share?url=https://game2016.videotel-cloud.com&hashtags=videotel,game2016&text=Videotel's Christmas Game&source=webclient");
            document.getElementById("fb-start").setAttribute("href", "https://www.facebook.com/sharer/sharer.php?u=https://game2016.videotel-cloud.com");
            this.hud = document.getElementById("hud");
            Main.makeCountryList();
        };
        Main.prototype.touchTest = function () {
            this._isTouch = 1;
            this.gameController.isTouchDevice = true;
            window.removeEventListener('touchstart', this.touchTest);
            document.getElementById("controls").classList.remove("hidden");
        };
        Main.prototype.sceneReadyHanlder = function (e) {
            this.hud.classList.remove("hidden");
            this.gameController.initLevel(e.data.scene, e.data.engine, e.data.data);
            this.soundManager.init(e.data.scene);
            this.time0 = Date.now();
            this.interval = 0;
            this.tTimer = requestAnimationFrame(this.updateTime);
            T._(this.overlayDiv, .5, [{ prop: "translateY", from: "0vh", to: "-100vh" }, { prop: "scaleZ", to: 1 }], { delay: 1 });
        };
        Main.prototype.pointsChangeHandler = function (e) {
            this.points += e.data;
            this.updateStatus();
            if (this.points <= 0) {
                this.points = 0;
                this.gameController.pause();
                document.querySelector("#gameFail span[data-id='time']").innerHTML = this.getTimeStr();
                document.querySelector("#gameFail span[data-id='score']").innerHTML = this.points + "";
                this.setScreen("gameFail");
                this.soundManager.dispose();
                this.showOverlay();
            }
        };
        Main.prototype.updateStatus = function () {
            var _this = this;
            T._(this, .5, [{ prop: "pointsCounter", to: this.points }], {
                onUpdate: function () {
                    _this.scoreHtml.innerHTML = Math.ceil(_this.pointsCounter) + "";
                    if (_this.points <= 80 && _this.points > 50) {
                        _this.scoreHtml.classList.add("warning");
                    }
                    else if (_this.points <= 50) {
                        _this.scoreHtml.classList.add("danger");
                    }
                    else {
                        _this.scoreHtml.classList.remove("danger");
                        _this.scoreHtml.classList.remove("warning");
                    }
                }
            });
        };
        Main.prototype.updateTime = function () {
            this.interval++;
            if (this.interval === 60) {
                this.interval = 0;
                this.time = Date.now() - this.time0;
                this.timeHtml.innerHTML = this.getTimeStr();
            }
            this.tTimer = requestAnimationFrame(this.updateTime);
        };
        Main.prototype.getTimeStr = function () {
            this.date.setTime(this.time);
            var mins = this.date.getMinutes();
            var secs = this.date.getSeconds();
            var mStr = mins > 9 ? mins + "" : "0" + mins;
            var sStr = secs > 9 ? secs + "" : "0" + secs;
            return mStr + ":" + sStr;
        };
        Main.prototype.levelCompleteHandler = function (e) {
            this.hud.classList.add("hidden");
            this.gameController.pause();
            this.soundManager.dispose();
            if (this.currentLevel < this.totalLevels) {
                this.levels.push({ points: this.points, time: this.time });
                document.querySelector("#levelComplete span[data-id='time']").innerHTML = this.getTimeStr();
                document.querySelector("#levelComplete span[data-id='score']").innerHTML = this.points + "";
                this.setScreen("levelComplete");
                document.querySelector("span[data-id=\"l-" + this.currentLevel + "\"]").classList.add("l-complete");
            }
            else {
                this.levels.push({ points: this.points, time: this.time });
                this.setScore();
                document.querySelector("#gameComplete span[data-id='score']").innerHTML = this.score + "";
                this.setScreen("gameComplete");
            }
            this.showOverlay();
        };
        Main.prototype.setScore = function () {
            var time = 0;
            var points = 0;
            for (var i = 0; i < this.levels.length; i++) {
                time += this.levels[i].time;
                points += this.levels[i].points;
            }
            this.score = Math.round(points / (time / 2000) * 300);
        };
        Main.prototype.btnClickHandler = function (e) {
            var targ = e.target;
            var id = targ.getAttribute("data-id");
            if (!id || targ.getAttribute("data-disabled")) {
                return;
            }
            this.points = 100;
            this.updateStatus();
            this.gameController.destroy();
            document.getElementById("overlay").scrollTop = 0;
            switch (id) {
                case "start":
                    this.setScreen("loader");
                    this.assetManager.loadScene("shipyard.json", "assets/level_1.json");
                    document.body.scrollTop = 0;
                    break;
                case "next":
                    this.gameController.destroy();
                    if (this.currentLevel < this.totalLevels) {
                        this.setScreen("loader");
                        this.currentLevel++;
                        this.assetManager.loadScene("shipyard.json", "assets/level_" + this.currentLevel + ".json");
                    }
                    break;
                case "replay":
                    window.location.reload();
                    break;
                case "fill-form":
                    this.setScore();
                    this.setScreen("sendScore");
                    this.prefillForm();
                    break;
                case "send-score":
                    document.querySelector("#scoreboard button[data-id='replay']").classList.remove("hidden");
                    document.querySelector("#scoreboard button[data-id='back']").classList.add("hidden");
                    this._isPolling = false;
                    this.sendScore();
                    break;
                case "help":
                    this.setScreen("help");
                    break;
                case "back":
                    this.setScreen("gameStart");
                    this._canPoll = false;
                    break;
                case "t-a-c":
                    this.setScreen("tac");
                    break;
                case "back-2-form":
                    this.setScreen("sendScore");
                    this._canPoll = true;
                    break;
                case "show-scores":
                    this.service.getScores();
                    this._canPoll = true;
                    this._isPolling = false;
                    this.prefillForm();
                    document.querySelector("#scoreboard button[data-id='replay']").classList.add("hidden");
                    document.querySelector("#scoreboard button[data-id='back']").classList.remove("hidden");
                    break;
            }
            var title = Main.level_titles[this.currentLevel - 1];
            document.getElementById("level-title").innerHTML = title;
            document.title = title;
        };
        Main.prototype.setScreen = function (screen) {
            var divs = this.overlayDiv.querySelectorAll(".res");
            for (var i = 0; i < divs.length; i++) {
                divs[i].classList.add("hidden");
            }
            this.overlayDiv.querySelector("#" + screen).classList.remove("hidden");
        };
        Main.prototype.showOverlay = function () {
            T._(this.overlayDiv, .5, [{ prop: "translateY", to: "0vh" }, { prop: "scaleZ", to: 1 }], { delay: 1 });
            window.cancelAnimationFrame(this.tTimer);
        };
        Main.prototype.sendScore = function () {
            var fname = document.querySelector("input[data-id='fname']");
            var femail = document.querySelector("input[data-id='email']");
            var tick = document.querySelector("input[data-id='tc-check']");
            var ok = true;
            fname.classList.remove("invalid");
            femail.classList.remove("invalid");
            document.querySelector(".agreement").classList.remove("invalid");
            femail.value = femail.value.toLowerCase();
            fname.value = fname.value.trim();
            if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,8})+$/.test(femail.value)) {
                femail.classList.add("invalid");
                ok = false;
            }
            if (fname.value.length < 2 || !vt.Lists.check(fname.value)) {
                fname.classList.add("invalid");
                ok = false;
            }
            if (!tick.checked) {
                document.querySelector(".agreement").classList.add("invalid");
                ok = false;
            }
            if (ok) {
                this.currentUser = femail.value;
                var str = "";
                this.fieldsData = [];
                var fields = document.getElementById("sendScore").querySelectorAll("input[type=text], input[type=email]");
                var id;
                for (var i = 0; i < fields.length; i++) {
                    var f = fields[i];
                    var name = f.getAttribute("data-id");
                    var value = f.value;
                    value = value.replace(/[^\w\W.@\s\-_&]/gmiu, "");
                    value = value.replace(/['"]/g, "");
                    value = encodeURIComponent(value);
                    str += "'" + value + "',";
                    if (name === "email") {
                        id = value;
                    }
                    this.fieldsData.push({ field: name, value: value });
                }
                var sel = document.querySelector("select[data-id='country']");
                var country = sel.selectedOptions[0].value;
                this.fieldsData.push({ field: "country", value: sel.selectedIndex });
                str += this.score;
                str += ", " + this._isTouch;
                str += ", '01010001100'";
                str += ", '" + encodeURIComponent(country) + "'";
                var query = "INSERT INTO t1 (fname, email, company, vessel, rank, score, touchDevice, ip, country) values (" + str + ")";
                var btn = document.querySelector("button[data-id='send-score']");
                btn.setAttribute("data-disabled", "true");
                btn.classList.add("disabled");
                this.service.setLocalData(this.fieldsData);
                this.service.sendScore(query);
                this.setSocialLinks();
                this._canPoll = true;
            }
        };
        Main.prototype.serverResponseHandler = function (e) {
            var _this = this;
            switch (e.data.type) {
                case VTEvent.SERVER_COMPETITION_EXPIRED:
                    this._competionEnded = true;
                    var btn = document.querySelector("button[data-id='fill-form']");
                    btn.setAttribute("data-disabled", "true");
                    btn.classList.add("disabled");
                    document.querySelector(".competition-ended").classList.remove("hidden");
                    break;
                case VTEvent.SERVER_SCORE_SENT:
                    this.service.getScores();
                    break;
                case VTEvent.SERVER_COMPETITION_RESULT:
                    var res = e.data.result;
                    var div = document.getElementById("game-result");
                    var str = "<div class='res-row'><span>Rank</span><span>Name</span><span>Best Score</span></div>";
                    for (var i = 0; i < res.length; i++) {
                        var field = res[i];
                        var userHere = (field.email === this.currentUser) ? "user" : "";
                        str += "<div class=\"res-row " + userHere + "\"><span>" + (i + 1) + "</span><span>" + field.name + "</span><span>" + field.score + "</span></div>";
                    }
                    div.innerHTML = str;
                    if (!this._isPolling) {
                        T._(this.overlayDiv, .5, [{ prop: "translateY", to: "-100vh" }], {
                            call: function () {
                                _this.setScreen("scoreboard");
                                _this.showOverlay();
                                _this._isPolling = true;
                            }
                        });
                    }
                    if (this._canPoll) {
                        setTimeout(function () {
                            _this.service.getScores();
                            console.log("--polling");
                        }, 5000);
                    }
                    break;
            }
        };
        Main.prototype.setSocialLinks = function () {
            var shareTxt = encodeURIComponent("I've just played Terminal Run! My score is " + this.score);
            document.getElementById("twit-end")["href"] = "https://twitter.com/share?url=https://game2016.videotel-cloud.com&hashtags=videotel,game2016&text=I've just played Terminal Run! My score is " + this.score + "&source=webclient";
            document.getElementById("fb-end").setAttribute("href", "https://www.facebook.com/sharer/sharer.php?u=https://game2016.videotel-cloud.com");
        };
        Main.prototype.prefillForm = function () {
            var ld = this.service.getLocalData();
            if (ld) {
                for (var i = 0; i < ld.length; i++) {
                    var o = ld[i];
                    var value = decodeURIComponent(o.value);
                    if (o.field === "email") {
                        this.currentUser = value;
                    }
                    if (o.field !== "country") {
                        document.querySelector("input[data-id=" + o.field + "]")["value"] = value;
                    }
                    else {
                        var sel = document.querySelector("select[data-id='country']");
                        sel.selectedIndex = o.country;
                        sel.options[o.value].setAttribute("selected", "");
                    }
                }
            }
        };
        Main.makeCountryList = function () {
            var l = vt.Lists.COUNTRY_LIST;
            var str = "<option value=\"\"></option>";
            for (var i = 0; i < l.length; i++) {
                var c = l[i];
                str += "<option value=\"" + c + "\">" + c + "</option>";
            }
            document.querySelector("select[data-id='country']").innerHTML = str;
        };
        Main.level_titles = [
            "TERMINAL START",
            "TERMINAL ARENA",
            "TERMINAL RUN",
        ];
        return Main;
    }());
    vt.Main = Main;
})(vt || (vt = {}));
