<?php
header('Access-Control-Allow-Origin: *');
header('Content-type:application/json;charset=utf-8');

require "db.php";


$func = $_POST["func"];
$nq = " WHERE email NOT LIKE '%videotel.com' AND email NOT LIKE '%kvh.com' AND company NOT LIKE 'placeholder' ";

switch ($func) {

    case "checkStatus" :
        checkStatus();
        break;

    case "sendScore" :
        sendScore($_POST["query"], $servername, $username, $password);
        break;

    case "getResults" :
        getResults($servername, $username, $password);
        break;

    case "getMain" :
        getMain($servername, $username, $password, $_POST["q"]);
        break;

    case "getAll" :
        getAll($servername, $username, $password, $_POST["q"]);
        break;

    default :
        echo "No function found";

}


function checkStatus()
{
    $expt = mktime(0,0,0, 1, 7, 2017);
    $t = time();
    if ($t > $expt) {
        sendResponse("competitionExpired", "true"); // competition expired
    } else {
        sendResponse("competitionExpired", "false"); // competition is still on!
    }
}

function sendScore($query, $servername, $username, $password)
{

    $conn = new mysqli($servername, $username, $password);
    if ($conn->connect_error) {
        $err = $conn->connect_error;
        sendResponse("error", "\"$err\"");
        die();
    }

    mysqli_select_db($conn, "game2016");

    $ip = $_SERVER['REMOTE_ADDR'];

    $query = str_replace("01010001100", $ip, $query);

    $statement = $conn->prepare($query);
    $statement->execute();
    $statement->store_result();

    if ($statement->affected_rows > 0) {
        sendResponse("dbInsert", '"ok"');
    } else {
        $err = urlencode($statement->error);
        sendResponse("error", "\"$err\"");
    }
    $statement->close();
    $conn->close();
}

function getResults($servername, $username, $password)
{

    $conn = new mysqli($servername, $username, $password);
    if ($conn->connect_error) {
        $err = $conn->connect_error;
        sendResponse("error", "\"$err\"");
        die();
    }
    $conn->set_charset("utf8");
    mysqli_select_db($conn, "game2016");

        $res = mysqli_query($conn, "select fname, MAX(score) as score, email from t1 GROUP BY email ORDER BY score DESC LIMIT 10;");
        $arr = array();
        while ($row = mysqli_fetch_assoc($res)) {
            $name = $row['fname'];
            $score = $row['score'];
            $email = $row['email'];
            array_push($arr, array('name'=>$name, 'score'=>$score, 'email'=>$email));
        }
        sendResponse("dbResult", json_encode($arr));


    $conn->close();
}



function sendResponse($type, $response)
{
    $resp = '"response":'.$response;
   echo ("{\"type\":\"$type\", $resp}");
}


function getMain($servername, $username, $password, $qualified) {

    $conn = new mysqli($servername, $username, $password);
    $conn->set_charset("utf8");
    mysqli_select_db($conn, "game2016");

    global $nq;
    $q = $qualified == 1? $nq : "";

    $res = mysqli_query($conn, "select fname, email, company, country, MAX(score) as max_score, COUNT(email) as num_plays from t1 $q GROUP BY email ORDER BY score DESC;");
    $arr = array();

    while ($row = mysqli_fetch_assoc($res)) {
        array_push($arr, array($row['fname'], $row['email'], $row['company'], $row['country'], $row['max_score'], $row['num_plays']));
    }
    sendResponse("qMain", json_encode($arr));
    $conn->close();
}

function getAll($servername, $username, $password, $qualified) {
    $conn = new mysqli($servername, $username, $password);
    $conn->set_charset("utf8");
    mysqli_select_db($conn, "game2016");
    global $nq;
    $q = $qualified == 1? $nq : "";
    $res = mysqli_query($conn, "select * from t1 $q GROUP BY email ORDER BY score DESC;");
    $arr = array();

    while ($row = mysqli_fetch_assoc($res)) {
        array_push($arr, array($row['fname'], $row['email'], $row['company'], $row['vessel'], $row['rank'], $row['country'], $row['touchDevice'], $row['ip'], $row['score'], $row['date']));
    }
    sendResponse("qAll", json_encode($arr));
    $conn->close();
}




?>





