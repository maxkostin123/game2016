/**
 * Created by max.kostin on 21/05/2015
 */

"use strict";

const gulp = require("gulp");
const ts = require("gulp-typescript");
const rename = require("gulp-rename");
const autoprefixer = require("gulp-autoprefixer");
const fs = require("fs-extra");
const uglify = require("gulp-uglify");

const tsProject = ts.createProject('tsconfig.json');

const browserSync = require("browser-sync");


/* =====================================================================================
 MAIN
 ======================================================================================*/
gulp.task('default', ['browserSync', 'ts_main', 'watch']);



/* =====================================================================================
 TYPESCRIPT
 ======================================================================================*/

gulp.task("ts_main", function () {
    clear();
    let tsRes = gulp.src("./src/Main.ts")
        .pipe(tsProject());
    return tsRes.js.pipe(gulp.dest("./bin/js/"))
        .pipe(browserSync.reload({stream: true}));
});


/* =====================================================================================
 CLEARS CONSOLE WINDOW
 ======================================================================================*/
function clear() {
    process.stdout.write("\u001b[2J\u001b[0;0H");
}


/* =====================================================================================
 BROWSER SYNC
 ======================================================================================*/
gulp.task("browserSync", function () {
    clear();
    browserSync({
        server: {
            baseDir: './bin'
        }
    })
});


gulp.task("uglify", ()=> {

    gulp.src("FILES")
        .pipe(uglify())
        .pipe(gulp.dest("DEST"))
        .on("end", ()=> {

        });
});

gulp.task("release", ()=> {

    fs.emptyDirSync("release");

    fs.copy("./bin", "./release", ()=> {
        let fStr = fs.readFileSync("./release/index.html").toString();
        fStr = fStr.replace("js/babylon.2.5.max.js", "js/babylon.js");
        fs.writeFileSync("./release/index.html", fStr);
        console.log("copied files.");
        gulp.src(["bin/js/Tweak.js", "bin/js/main.js", "bin/js/pep.js"])
            .pipe(uglify())
            .pipe(gulp.dest("./release/js/"))
            .on("end", ()=> {
                console.log("minified js.");
            });
    })

});





/* =====================================================================================
 WATCH
 ======================================================================================*/
gulp.task('watch', function () {
    gulp.watch('./src/**/*.ts', ['ts_main']);

});


/* =====================================================================================
 TESTS
 ======================================================================================*/








